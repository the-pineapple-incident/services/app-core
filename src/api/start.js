import { ApolloServer } from 'apollo-server';

import config from '../config';
import getSchema from './graph/schema';

const apiManager = {

  async startServer() {
    // Config
    const { port, enablePlayground, enableIntrospection } = config.api;

    const schema = await getSchema();
    const server = new ApolloServer({
      schema,
      playground: enablePlayground,
      introspection: enableIntrospection,
    });

    //Server
    const { url } = await server.listen({ port });
    console.log(`🚀 API running in ${url}`);
  },
};

export default apiManager;
