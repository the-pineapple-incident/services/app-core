import DateTimeType from './dateTimeType';
import DateType from './dateType';

const typedefResolver = {
  DateTime: DateTimeType,
  Date: DateType,
};

export default typedefResolver;
