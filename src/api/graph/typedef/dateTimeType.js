import { GraphQLScalarType, Kind } from 'graphql';

export default new GraphQLScalarType({
  name: 'DateTime',
  description: 'DateTime custom type',
  parseValue(value) {
    return new Date(value);
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 19));
    }
    return null;
  },
  serialize(value) {
    const date = new Date(value);
    return date.toISOString();
  },
});
