import { GraphQLScalarType, Kind } from 'graphql';

export default new GraphQLScalarType({
  name: 'Date',
  description: 'Date custom type',
  parseValue(value) {
    return new Date(value);
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10));
    }
    return null;
  },
  serialize(value) {
    const date = new Date(value);
    return date.toISOString().substring(0, 10);
  },
});
