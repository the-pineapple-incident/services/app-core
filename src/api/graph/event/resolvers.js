import eventService from '../../../domain/admin/event/event-service';
import opinionService from '../../../domain/admin/opinion/opinion-service';

const eventResolver = {
  Mutation: {
    addEvent: (root, { event }) => eventService.register(event),
  },
  Query: {
    activeEvents: () => eventService.getAllActiveEvents(),
    saveExtraEvents: () => eventService.saveAllEvents(),
  },
  Event: {
    opinions: (event) => opinionService.getAllOpinionsOfEvent(event),
  },
};

export default eventResolver;
