import topicService from '../../../domain/admin/topic/topic-service';
import themeService from '../../../domain/admin/theme/theme-service';

const topicResolver = {
  Mutation: {
    addTopic: (root, { idDimension, topic }) => topicService.addTopic(idDimension, topic),
  },
  Topic: {
    themes: (topic) => themeService.getThemesOfTopic(topic.name),
  },
};

export default topicResolver;
