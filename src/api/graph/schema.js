import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { loadSchema } from '@graphql-tools/load';
import { addResolversToSchema } from '@graphql-tools/schema';
import condensedResolver from './resolvers';

async function getSchema() {
  const schema = await loadSchema('./**/*.graphql', {
    loaders: [new GraphQLFileLoader()],
  });

  return addResolversToSchema({
    schema,
    resolvers: condensedResolver,
  });
}

export default getSchema;
