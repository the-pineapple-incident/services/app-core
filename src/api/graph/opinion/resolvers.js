import eventService from '../../../domain/admin/event/event-service';
import themeService from '../../../domain/admin/theme/theme-service';
import opinionService from '../../../domain/admin/opinion/opinion-service';

const opinionResolver = {
  Mutation: {
    addOpinionToEvent: (root, { opinion, idEvent }) => eventService.addOpinion(opinion, idEvent),
    addOpinionToTheme: (root, { opinion, idDimension, topicName, themeName }) => themeService.addOpinionToTheme(opinion, idDimension, topicName, themeName),
  },
  Query: {
    opinionsOf: (root, { idPoliticFigure }) => opinionService.getAllOpinionsOfPoliticFigure(idPoliticFigure),
    saveExtraEventOpinions: () => opinionService.saveAllExtraEventOpinions(),
  },
};

export default opinionResolver;
