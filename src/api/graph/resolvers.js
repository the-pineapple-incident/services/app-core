import { mergeResolvers } from '@graphql-tools/merge';
import politicalFigureResolver from './political-figure/resolvers';
import politicalPartyResolver from './political-party/resolvers';
import politicalEventResolver from './political-event/resolvers';
import eventResolver from './event/resolvers';
import topicResolver from './topic/resolvers';
import typedefResolver from './typedef/resolvers';
import dimensionResolver from './dimension/resolvers';
import themeResolver from './theme/resolvers';
import opinionResolver from './opinion/resolvers';

const resolvers = [
  politicalPartyResolver,
  politicalFigureResolver,
  politicalEventResolver,
  eventResolver,
  typedefResolver,
  topicResolver,
  dimensionResolver,
  themeResolver,
  opinionResolver,
];

const condensedResolver = mergeResolvers(resolvers);

export default condensedResolver;
