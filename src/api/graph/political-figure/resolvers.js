import politicalFigureService from '../../../domain/political-figure/political-figure-service';
import personService from '../../../domain/person/person-service';

const politicalFigureResolver = {
  Query: {
    politicalFigures: () => politicalFigureService.getAllPoliticalFigures(),
    politicalFiguresOfPartyAndEvent: (root, { idParty, idEvent }) => politicalFigureService.getAllPoliticalFiguresOf(idParty, idEvent),
    politicalFigure: (root, { id }) => politicalFigureService.getPoliticalFigure(id),
  },
  PoliticalFigure: {
    person: (politicalFigure) => personService.getPersonOf(politicalFigure),
  },
  Candidacy: {
    politicalParty: (root) => politicalFigureService.getPartyOfFigure(root.politicalPartyId),
    politicalEvent: (root) => politicalFigureService.getEventOfFigure(root.politicalEventId),
    applicationPlace: (root) => politicalFigureService.getPlaceOfFigure(root.applicationPlaceId),
  },
};

export default politicalFigureResolver;
