import politicalEventService from 'domain/political-event/political-event-service';
import politicalFigureService from 'domain/political-figure/political-figure-service';
import partyService from 'domain/parties/party-service';

const politicalEventResolver = {
  Query: {
    activePoliticalEvents: () => politicalEventService.getAllActivePoliticalEvents(),
    participatingParties: (_, { eventId }) => politicalEventService.getParticipatingPartiesOfEvent(eventId),
  },
  PoliticalEvent: {
    politicalFigures: (politicalEvent) => politicalFigureService.getAllPoliticalFiguresOfEvent(politicalEvent),
    participants: ({ participants }, _) => partyService.getAllPartiesWithPlans(participants),
  },
};

export default politicalEventResolver;
