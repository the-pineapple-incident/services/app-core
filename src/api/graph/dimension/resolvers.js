import dimensionService from '../../../domain/admin/dimension/dimension-service';
import topicService from '../../../domain/admin/topic/topic-service';

const dimensionResolver = {
  Mutation: {
    addDimension: (root, { dimension }) => dimensionService.addDimension(dimension),
  },
  Query: {
    dimensions: () => dimensionService.findAll(),
  },
  Dimension: {
    topics: (dimension) => topicService.getTopicsOfDimension(dimension.id),
  },
};

export default dimensionResolver;
