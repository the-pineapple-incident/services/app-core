import themeService from '../../../domain/admin/theme/theme-service';
import opinionService from '../../../domain/admin/opinion/opinion-service';

const themeResolver = {
  Mutation: {
    addTheme: (root, { idDimension, topicName, theme }) => themeService.addTheme(idDimension, topicName, theme),
  },
  Theme: {
    opinions: (theme) => opinionService.getAllOpinionsOfTheme(theme.name),
  },
};

export default themeResolver;
