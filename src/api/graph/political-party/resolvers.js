import partyService from '../../../domain/parties/party-service';

const politicalPartyResolver = {
  Query: {
    politicalParties: () => partyService.getAllParties(),
    politicalParty: (root, { id }) => partyService.findPartyByID({ id }),
    politicalPartiesInEvent: (root, { politicalEventId }) => partyService.getAllPartiesInEvent(politicalEventId),
  },
};

export default politicalPartyResolver;
