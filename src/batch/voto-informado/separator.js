import stampit from 'stampit';
import * as R from 'ramda';

const SeparatorStamp = stampit({
  methods: {
    separateByKeys(naturalKey, idsWithKeys, collection) {
      const mapped = R.indexBy(R.prop(naturalKey), idsWithKeys);

      const [withIds, withoutIds] = R.partition(R.pipe(
        R.prop(naturalKey),
        R.has(R.__, mapped),
      ))(collection);

      const assocKey = R.curry((value, obj) => R.assoc('_id', value, obj));

      const assoc = (e) => R.pipe(
        R.prop(naturalKey),
        R.prop(R.__, mapped),
        R.prop('_id'),
        assocKey(R.__, e),
      )(e);

      return {
        withoutKeys: R.map(assocKey(null))(withoutIds),
        withKeys: R.map(assoc)(withIds),
      };
    },
  },
});

export default SeparatorStamp;
