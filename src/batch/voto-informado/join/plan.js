import * as R from 'ramda';
import stampit from 'stampit';

const PersistGovernmentPlansStamp = stampit({
  props: {
    plans: null,
    indexed: null,
    planRepository: null,
  },

  init({ governmentPlans, governmentPlanRepository }) {
    this.plans = governmentPlans;
    this.planRepository = governmentPlanRepository;
  },

  methods: {
    async persist() {
      const toInsert = R.map(R.omit(['id', 'politicalPartyId']))(this.plans);
      const inserted = await this.planRepository.insertAll(toInsert);

      // indexing by id
      const ids = R.pluck('id', this.plans);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);

      // indexing by political party id
      const partyIds = R.pluck('politicalPartyId', this.plans);
      this.partyIndexed = R.compose(R.fromPairs, R.zip)(partyIds, inserted);
    },
    getObjectId(id) {
      return R.path([id, '_id'], this.indexed);
    },
    getObjectIdByPartyId(partyId) {
      return R.path([partyId, '_id'], this.partyIndexed);
    },
  },
});

export default PersistGovernmentPlansStamp;
