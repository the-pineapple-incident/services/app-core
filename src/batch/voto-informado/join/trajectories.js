import stampit from 'stampit';
import * as R from 'ramda';

const PersistTrajectoriesStamp = stampit({
  props: {
    trajectories: null,
    totalTrajectories: null,
    positionManager: null,
    indexedByPersonId: null,
  },

  init({ trajectories, positionManager, figureManager }) {
    this.trajectories = trajectories;
    this.positionManager = positionManager;
    this.figureManager = figureManager;
  },

  methods: {
    considerAdditionalTrajectories(trajectories) {
      this.totalTrajectories = [...this.trajectories, ...trajectories];
    },

    buildTrajectories() {
      const { positionManager, figureManager } = this;
      const makeTrajectory = (trajectory) => {
        const positionId = positionManager.getObjectId(trajectory.positionId);
        const politicalFigureId = figureManager.getObjectIdByRawPositionId(trajectory.positionId);
        return {
          positionId,
          politicalFigureId,
        };
      };

      const personIds = R.pluck('personId', this.totalTrajectories);
      const refactored = R.map(makeTrajectory, this.totalTrajectories);
      this.indexedByPersonId = R.pipe(
        R.zip,                  // build pairs           => [[id1, t1], [id1, t2], [id2, t3], ...]
        R.groupBy(R.head),      // group by personIds    => {id1: [[id1, t1], [id1, t2]], id2: [[id2, t3], ...], ...}
        R.map(R.map(R.last)),   // get only trajectories => {id1: [t1, t2], id2: [t3, ...], ...}
      )(personIds, refactored);
    },

    getTrajectoriesByPersonId(personId) {
      return R.prop(personId, this.indexedByPersonId);
    },
  },
});

export default PersistTrajectoriesStamp;
