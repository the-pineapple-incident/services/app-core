import * as R from 'ramda';
import stampit from 'stampit';

const PersistPartiesStamp = stampit({
  props: {
    parties: null,
    indexed: null,
    partyRepository: null,
    partiesWithIds: null,
  },

  init({ parties, partyRepository }) {
    this.parties = parties;
    this.partyRepository = partyRepository;
  },

  methods: {
    async getDBIdsByNames(names) {
      return await this.partyRepository.getIdsByNames(names);
    },
    async separate() {
      const names = R.pluck('name')(this.parties);
      const namesWithIDs = await this.getDBIdsByNames(names);
      const {
        withoutKeys: toInsert,
        withKeys: toUpdate,
      } = this.separateByKeys('name', namesWithIDs, this.parties);

      return {
        toInsert,
        toUpdate,
      };
    },
    getNamesByNaturalIds(ids) {
      const mapped =  R.indexBy(R.prop('id'), this.parties);
      const getName = (id) => R.path([id, 'name'], mapped);
      const toTuple = (id) => [id, getName(id)];

      return R.pipe(
        R.map(toTuple),
        R.fromPairs,
      )(ids);
    },
    async persist() {
      const withoutIds = R.map(R.omit(['id']))(this.parties);
      const inserted = await this.partyRepository.insertAll(withoutIds);

      // indexing
      const ids = R.pluck('id', this.parties);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);
    },
    getObjectId(id) {
      return R.path([id, '_id'], this.indexed);
    },
  },
});

export default PersistPartiesStamp;
