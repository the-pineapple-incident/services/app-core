import stampit from 'stampit';
import * as R from 'ramda';

const PersistIncomesStamp = stampit({
  props: {
    incomes: null,
  },

  init({ incomes }) {
    this.incomes = incomes;
  },

  methods: {
    normalize(collection) {
      return R.pipe(
        R.indexBy(R.prop('politicalFigureId')),
        R.map(R.omit(['politicalFigureId', 'id'])),
      )(collection);
    },
    buildIncomes() {
      this.indexed = this.normalize(this.incomes);
    },
    getIncomesForFigure(id) {
      return R.prop(id, this.indexed) ?? null;
    },
  },
});

export default PersistIncomesStamp;
