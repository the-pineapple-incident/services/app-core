import stampit from 'stampit';
import * as R from 'ramda';

const PersistAnnotationsStamp = stampit({
  props: {
    annotations: null,
    indexed: null,
  },

  init({ annotations }) {
    this.annotations = annotations;
  },

  methods: {
    normalize(collection) {
      const groupByFigure = R.groupBy(R.prop('politicalFigureId'), collection);
      return R.map(R.map(R.omit(['politicalFigureId', 'id'])))(groupByFigure);
    },
    buildAnnotations() {
      this.indexed = this.normalize(this.annotations);
    },
    getAnnotationsForFigure(id) {
      return R.prop(id, this.indexed) ?? [];
    },
  },
});

export default PersistAnnotationsStamp;
