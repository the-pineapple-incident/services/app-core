import stampit from 'stampit';
import * as R from 'ramda';

const PersistGoodsStamp = stampit({
  props: {
    goods: null,
    movables: null,
    immovables: null,
    others: null,
  },

  init({ movables, immovables, others }) {
    this.movables = movables;
    this.immovables = immovables;
    this.others = others;
  },

  methods: {
    normalize(collection) {
      const groupByFigure = R.groupBy(R.prop('politicalFigureId'), collection);
      return R.map(R.map(R.omit(['politicalFigureId', 'id'])))(groupByFigure);
    },
    buildGoods() {
      const movables = this.normalize(this.movables);
      const immovables = this.normalize(this.immovables);
      const others = this.normalize(this.others);
      this.goods = { movables, immovables, others };
    },
    getGoodsForFigure(id) {
      return {
        movables: R.path(['movables', id], this.goods) ?? [],
        immovables: R.path(['immovables', id], this.goods) ?? [],
        others: R.path(['others', id], this.goods) ?? [],
      };
    },
  },
});

export default PersistGoodsStamp;
