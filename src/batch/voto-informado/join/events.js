import stampit from 'stampit';
import * as R from 'ramda';
import SeparatorStamp from 'batch/voto-informado/separator';

const PersistEventsStamp = stampit(SeparatorStamp, {
  props: {
    events: null,
    nonUniqueIndexed: null,
    indexed: null,
    eventsRepository: null,
  },

  init({ events, politicalEventRepository, partyManager }) {
    this.events = events;
    this.eventsRepository = politicalEventRepository;
    this.partyManager = partyManager;
  },

  methods: {
    deleteRepeated(events) {
      const sameName = R.eqProps('name');
      return R.uniqWith(sameName)(events);
    },
    indexNonRepeated() {
      const ids = R.pluck('id', this.events);
      this.nonUniqueIndexed = R.compose(R.fromPairs, R.zip)(ids, this.events);
    },
    registerParticipantsInCurrentEvent(currentEventId, partyIds, planManager) {
      const { partyManager } = this;
      // get the current political event (by now I code this, April/2021)
      const rawIndexed = R.indexBy(R.prop('id'), this.events);
      const currentEvent = R.prop(currentEventId, rawIndexed);

      // build participant party in current event
      const buildParticipant = (partyId) => ({
        politicalPartyId: partyManager.getObjectId(partyId),
        governmentPlanId: planManager.getObjectIdByPartyId(partyId),
      });

      const participants = R.map(buildParticipant, partyIds);

      // insert party modified with participants into the current raw array of parties
      const completed = R.assoc(currentEventId, { ...currentEvent, participants }, rawIndexed);
      this.events = R.values(completed);
    },
    async persist() {
      this.indexNonRepeated();
      const uniques = this.deleteRepeated(this.events);
      const events = R.map(R.omit(['id']))(uniques);
      const inserted = await this.eventsRepository.insertAll(events);

      // indexing
      const ids = R.pluck('id', uniques);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);
    },
    getObjectId(id) {
      return R.path([id, '_id'], this.indexed);
    },
    getObjectIdByNameOfOtherSimilarEvent(id) {
      const name = R.path([id, 'name'], this.nonUniqueIndexed);
      return R.pipe(
        R.values,
        R.find(R.propEq('name', name)),
        R.prop('_id'),
      )(this.indexed);
    },
  },
});

export default PersistEventsStamp;
