import * as R from 'ramda';
import stampit from 'stampit';

const PersistPlacesStamp = stampit({
  props: {
    places: null,
    indexed: null,
    placeRepository: null,
  },

  init({ places, placeRepository }) {
    this.places = places;
    this.placeRepository = placeRepository;
  },

  methods: {
    async persist() {
      const withoutIds = R.map(R.omit(['id']))(this.places);
      const inserted = await this.placeRepository.insertAll(withoutIds);

      // indexing
      const ids = R.pluck('id', this.places);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);
    },
    getObjectId(id) {
      if (R.isNil(id)) {
        return null;
      }

      return R.path([id, '_id'], this.indexed);
    },
  },
});

export default PersistPlacesStamp;
