import * as R from 'ramda';
import stampit from 'stampit';
import SeparatorStamp from 'batch/voto-informado/separator';

const PersistPositionsStamp = stampit(SeparatorStamp, {
  props: {
    positions: null,
    processedPositions: null,
    positionRepository: null,
    partyManager: null,
    figureManager: null,
  },

  init({ positions, positionRepository, partyManager, figureManager }) {
    this.positions = positions;
    this.positionRepository = positionRepository;
    this.partyManager = partyManager;
    this.figureManager = figureManager;
  },

  methods: {
    reprocess() {
      const { partyManager } = this;
      const changeParty = (position) => {
        const politicalPartyId = partyManager.getObjectId(position.politicalPartyId);
        return R.assoc('politicalPartyId', politicalPartyId, position);
      };

      const refactor = R.compose(R.omit(['id', 'id2']), changeParty);
      return R.map(refactor, this.positions);
    },

    createPoliticalFigures(positions) {
      return null;
    },

    createTrajectories(positions) {
      const { figureManager } = this;
      const makeTrajectory = (position) => {
        const figure = figureManager.getRawFigureByPositionId(position.id);
        if (R.isNil(figure)) return null;

        return {
          personId: figure.personId,
          politicalEventId: figure.candidacy?.politicalEventId,
          positionId: position.id,
        };
      };

      return R.map(makeTrajectory, positions);
    },

    generateMissedObjects() {
      const hasTrajectoryId = (obj) => R.compose(R.startsWith('trayectoria_'), R.prop('id2'))(obj);
      const [withTrajectory, withoutTrajectory] = R.partition(hasTrajectoryId, this.positions);
      const figures = this.createPoliticalFigures(withTrajectory);
      const trajectories = this.createTrajectories(withoutTrajectory);
      return { figures, trajectories };
    },

    async persist() {
      const positions = this.reprocess();
      const inserted = await this.positionRepository.insertAll(positions);

      // indexing
      const ids = R.pluck('id', this.positions);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);
    },

    getObjectId(id) {
      return R.path([id, '_id'], this.indexed);
    },
  },
});

export default PersistPositionsStamp;
