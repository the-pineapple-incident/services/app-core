import stampit from 'stampit';
import * as R from 'ramda';

const PersistFiguresStamp = stampit({
  props: {
    figures: null,
    refactored: null,
    indexed: null,
    figureRepository: null,
    goodsManager: null,
    incomesManager: null,
    annotationsManager: null,
    dependencies: null,
  },

  init({ politicalFigures, goodsManager, incomesManager, annotationsManager, politicalFigureRepository, dependencies }) {
    this.figures = politicalFigures;
    this.goodsManager = goodsManager;
    this.incomesManager = incomesManager;
    this.annotationsManager = annotationsManager;
    this.figureRepository = politicalFigureRepository;
    this.dependencies = dependencies;
  },

  methods: {
    currySolveCandidacyIds() {
      const { partyManager, eventManager, placeManager } = this.dependencies;
      return R.curry((obj) => {
        const { politicalPartyId, politicalEventId, applicationPlaceId, applicationDate } = obj.candidacy;
        const candidacy = {
          politicalPartyId: partyManager.getObjectId(politicalPartyId),
          politicalEventId: eventManager.getObjectIdByNameOfOtherSimilarEvent(politicalEventId),
          applicationPlaceId: placeManager.getObjectId(applicationPlaceId),
          applicationDate,
        };
        return { ...obj, candidacy };
      });
    },
    curryRefactorReported() {
      const { incomesManager, annotationsManager, goodsManager } = this;
      return R.curry((obj) => {
        const reported = {
          incomes: incomesManager.getIncomesForFigure(obj.id),
          mistakeAnnotations: annotationsManager.getAnnotationsForFigure(obj.id),
          additionalInfo: obj.reported.additionalInfo,
          goods: goodsManager.getGoodsForFigure(obj.id),
        };
        return { ...obj, reported };
      });
    },
    considerAdditionalFigures(figures) {
      const f = figures ?? [];
      this.totalRawFigures = [...this.figures, ...f];
    },
    buildFigures() {
      const solveCandidacyIds = this.currySolveCandidacyIds();
      const refactorReported = this.curryRefactorReported();
      const refactor = R.compose(solveCandidacyIds, refactorReported);
      this.refactored = R.map(refactor, this.totalRawFigures);
    },
    async persist() {
      // depends on refactored
      const toInsert = R.map(R.omit(['id', 'personId', 'positionId']), this.refactored);
      const inserted = await this.figureRepository.insertAll(toInsert);

      // indexing
      const ids = R.pluck('id', this.refactored);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);
    },
    getRawFigureByPositionId(positionId) {
      if (R.isNil(positionId)) {
        return null;
      }

      return R.find(R.propEq('positionId', positionId), this.figures);
    },
    getObjectId(figureId) {
      if (R.isNil(figureId)) {
        return null;
      }

      return R.path([figureId, '_id'], this.indexed);
    },
    getObjectIdByRawPositionId(rawPositionId) {
      const rawFigure = this.getRawFigureByPositionId(rawPositionId);
      return this.getObjectId(rawFigure?.id);
    },
    async addPersonIdsToAllFigures(personManager) {
      const {refactored, indexed} = this;
      const getPersonObjectId = (figureId) => {
        const personId = R.prop('personId', R.find(R.propEq('id', figureId), refactored));
        return personManager.getObjectId(personId);
      }

      const addPersonId = (_, figureId, figure) => {
        const a = R.assoc('personId', getPersonObjectId(figureId), figure);
        return a;
      }

      const figures = R.pipe(
        R.mapObjIndexed(addPersonId),
        R.values,
      )(indexed);

      await this.figureRepository.updateAll(figures);
    },
  },
});

export default PersistFiguresStamp;
