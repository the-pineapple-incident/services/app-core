import stampit from 'stampit';
import * as R from 'ramda';

const PersistPeopleStamp = stampit({
  props: {
    indexed: null,
    people: null,
    personRepository: null,
    placeManager: null,
    trajectoryManager: null,
  },

  init({ people, personRepository, placeManager, trajectoryManager, partyManager }) {
    this.people = people;
    this.personRepository = personRepository;
    this.placeManager = placeManager;
    this.trajectoryManager = trajectoryManager;
    this.partyManager = partyManager;
  },

  methods: {
    reprocess() {
      const { placeManager, trajectoryManager, partyManager } = this;

      const replaceWaiver = (waiver) => {
        const politicalPartyId = partyManager.getObjectId(waiver.politicalPartyId);
        return R.assoc('politicalPartyId', politicalPartyId, waiver);
      };

      const replaceWaivers = (person) => {
        const waivers = R.map(replaceWaiver, person.resume.waivers);
        return R.assocPath(['resume', 'waivers'], waivers, person);
      };

      const addPlaceId = R.curry((propName, person) => {
        const placeId = placeManager.getObjectId(R.path([propName, 'place'], person));
        return R.assocPath([propName, 'place'], placeId, person);
      });

      const addTrajectories = (person) => {
        const trajectories = trajectoryManager.getTrajectoriesByPersonId(person.id);
        return R.assocPath(['resume', 'trajectory'], trajectories, person);
      };

      const refactor = R.pipe(
        replaceWaivers,
        addPlaceId('dwelling'),
        addPlaceId('birth'),
        addTrajectories,
        R.omit(['id']),
      );
      return R.map(refactor, this.people);
    },
    async persist() {
      const people = this.reprocess();
      const inserted = await this.personRepository.insertAll(people);

      // indexing
      const ids = R.pluck('id', this.people);
      this.indexed = R.compose(R.fromPairs, R.zip)(ids, inserted);
    },
    getObjectId(personId) {
      if (R.isNil(personId)) {
        return null;
      }

      return R.path([personId, '_id'], this.indexed);
    },
  },
});

export default PersistPeopleStamp;
