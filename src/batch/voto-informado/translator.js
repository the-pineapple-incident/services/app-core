import stampit from 'stampit';
import * as R from 'ramda';

const TranslatorStamp = stampit({
  methods: {
    translate(to, obj) {
      const propsTranslated = R.values(to);
      const propsToTranslate = R.keys(to);

      return R.pipe(
        R.pick(propsToTranslate),
        R.values,
        R.zipObj(propsTranslated),
      )(obj);
    },

    translateCollection(to, collection) {
      const translate = R.curry(this.translate);
      return R.map(translate(to))(collection);
    },
  },
});

export default TranslatorStamp;
