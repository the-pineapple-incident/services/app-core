import * as R from 'ramda';

const castProp = R.curry((propName, cast, obj) => {
  const value = cast(R.prop(propName, obj));
  const propLens = R.lensProp(propName);
  return R.set(propLens, value, obj);
});

const castPropCollection = R.curry((propName, cast, collection) => R.map(castProp(propName, cast))(collection));

export { castProp, castPropCollection };
