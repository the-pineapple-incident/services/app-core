import * as R from 'ramda';

const translateProps = R.curry((obj, to) => {
  const propsTranslated = R.values(to);
  const propsToTranslate = R.keys(to);

  return R.pipe(
    R.pickAll(propsToTranslate),
    R.values,
    R.zipObj(propsTranslated),
  )(obj);
});

const translatePropsCollection = (collection, to) => R.map(translateProps(R.__, to), collection);

export { translateProps, translatePropsCollection };
