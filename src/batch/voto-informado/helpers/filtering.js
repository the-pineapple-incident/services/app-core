import * as R from 'ramda';
import { isNilOrEmpty } from 'batch/voto-informado/helpers/conditionals';

const filterNonPropValues = R.curry((propName, collection) => {
  const filterObj = (obj) => !isNilOrEmpty(R.prop(propName, obj));
  return R.filter(filterObj, collection);
});

export { filterNonPropValues };
