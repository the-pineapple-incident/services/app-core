import * as R from 'ramda';

/**
 * Create a new prop using another props. These props will be used to calculate the
 * new prop using the function passed, then, these props will be removed from the object
 * and the new prop will be added with its new calculated value.
 * @param {Object} obj - object to refactor
 * @param {Object} config - arguments used to generate the new prop and refactor
 *
 *  - newPropName: String - name of the new prop calculated
 *  - toExtract: Array[String] | String - prop names to extract, omit and used to calculate the new prop
 *  - makeWithExtract: Function - function applied to make the new prop using the extracted props
 *
 *  Example 1:
 *  - obj = {a, b, c: {d, e}}, add 'w' as 'a+b'
 *  - newObj = (obj, {newPropName: 'w', toExtract: ['a', 'b'], makeWithExtract: ({a, b}) => a + b})
 *  - newObj = {w, c: {d, e}}, when w=a+b
 *
 *  Example 2:
 *  - obj = {a, b, c: {d, e}}, add 'x' as 'c'
 *  - newObj = (obj, {newPropName: 'x', toExtract: 'c', makeWithExtract: (c) => {...c}})
 *  - newObj = {a, b, x: {d, e}}
 * */
const replacePropWithOthers = R.curry((obj, { newPropName, toExtract, makeWithExtract }) => {
  const extract = typeof toExtract === 'string' ? R.prop : R.pick;
  const purge = typeof toExtract === 'string' ? R.dissoc : R.omit;

  const purged = purge(toExtract, obj);
  const newProp = makeWithExtract(extract(toExtract, obj));
  return R.assoc(newPropName, newProp, purged);
});

const replacePropWithOthersCollection = (collection, {
  newPropName,
  toExtract,
  makeWithExtract,
}) => R.map(replacePropWithOthers(R.__, { newPropName, toExtract, makeWithExtract }))(collection);

const removeProps = R.curry((obj, toRemove) => R.omit(toRemove, obj));

export { replacePropWithOthers, replacePropWithOthersCollection, removeProps };
