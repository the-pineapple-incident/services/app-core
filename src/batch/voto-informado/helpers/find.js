import * as R from 'ramda';

const findAllByProp = R.curry((collection, value, propName) => {
  return R.filter(R.propEq(propName, value))(collection);
});

export { findAllByProp };
