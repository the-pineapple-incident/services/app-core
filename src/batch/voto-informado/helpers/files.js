import path from 'path';
import fs from 'fs';

export const getObjectFromFile = (filename) => {
  const placeFilePath = path.join(__dirname, '..', 'data', filename);
  return JSON.parse(fs.readFileSync(placeFilePath, 'utf8'));
};
