import * as R from 'ramda';

const omit = R.curry((props, obj) => R.omit(props, obj));
const omitCollection = R.curry((props, collection) => R.map(omit(props), collection));

export { omitCollection };
