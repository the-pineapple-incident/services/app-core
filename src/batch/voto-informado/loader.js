import stampit from 'stampit';
import path from 'path';
import fs from 'fs';
import batchConfig from 'config/batch';

const LoaderStamp = stampit({
  methods: {
    loadData(filename) {
      const filePath = path.join(batchConfig.path, filename);
      return JSON.parse(fs.readFileSync(filePath, 'utf8'));
    },
  },
});

export default LoaderStamp;
