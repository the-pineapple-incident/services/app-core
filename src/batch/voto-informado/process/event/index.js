import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';

const propsToTranslate = {
  'idProcesoElectoral': 'id',
  'nombreproceso': 'name',
  'estado': 'state',
  'anho': 'year',
};

const raws = getObjectFromFile('procesoElectoral.json');
const events = translatePropsCollection(raws, propsToTranslate);

console.log('loading entities: Events');
export { events };
