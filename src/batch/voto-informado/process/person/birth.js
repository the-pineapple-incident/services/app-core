
const createBirth = ({ birthdate, birthplaceId }) => ({
  date: birthdate,
  place: birthplaceId,
});

const replaceBirth = {
  newPropName: 'birth',
  toExtract: ['birthdate', 'birthplaceId'],
  makeWithExtract: createBirth,
};

export default replaceBirth;
