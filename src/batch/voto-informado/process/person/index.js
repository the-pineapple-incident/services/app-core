import * as R from 'ramda';
import { replacePropWithOthers } from '../../helpers/replacement';
import replaceBirth from './birth';
import replaceDwelling from './dwelling';
import replaceIdentity from './identity';
import processResume from './resume';
import processJudgment from './judgment';

import { getObjectFromFile } from '../../helpers/files';
import { translatePropsCollection } from '../../helpers/translator';

const propsToTranslate = {
  'id': 'id',
  'nombres': 'name',
  'apellidoMaterno': 'firstLastname',
  'apellidoPaterno': 'secondLastname',
  'sexo': 'gender',
  'urlFoto': 'profilePicture',
  'fechaNacimiento': 'birthdate',         // cover birth.date
  'idLugarNacimiento': 'birthplaceId',    // cover birth.place
  'idLugarResidencia': 'dwellingId',      // cover dwelling.place (ID)
  'dni': 'dni',                           // cover identity type DNI      => code
  'carneExtranjeria': 'passport',         // cover identity type PASSPORT => code
  'informacionCV': 'resume',              // {}
  'antecedentesLegales': 'judgment',      // {}
};

const raws = getObjectFromFile('persona.json');
const translated = translatePropsCollection(raws, propsToTranslate);

// # PURGE!
const purgePerson = R.pipe(
  replacePropWithOthers(R.__, replaceBirth),
  replacePropWithOthers(R.__, replaceDwelling),
  replacePropWithOthers(R.__, replaceIdentity),
  processResume,
  processJudgment,
);

console.log('loading entities: People - purging');
const people = R.map(purgePerson, translated);

console.log('loading entities: People - purged');
export { people };
