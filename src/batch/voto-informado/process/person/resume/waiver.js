import * as R from 'ramda';
import { findAllByProp } from '../../../helpers/find';
import { getObjectFromFile } from '../../../helpers/files';
import { translatePropsCollection } from '../../../helpers/translator';

const propsToTranslate = {
  'idRenuncia': 'id',
  'idPersona': 'personId',
  'idPartidoPolitico': 'politicalPartyId',
  'fechaRenuncia': 'date',
};

const raws = getObjectFromFile('renuncia.json');
const waivers = translatePropsCollection(raws, propsToTranslate);

const findPersonWaivers = findAllByProp(waivers, R.__, 'personId');

console.log('loading entities: Resume -> Waivers');
export { waivers, findPersonWaivers };
