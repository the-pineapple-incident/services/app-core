import * as R from 'ramda';
import { removeProps } from '../../../helpers/replacement';
import { findPersonEducations } from './education';
import { findPersonExperiences } from './experience';
import { findPersonWaivers } from './waiver';
import { omitCollection } from 'batch/voto-informado/helpers/omit';

const propsToRemove = [
  'idEducaciones',
  'idExperiencias',
  'idTrayectorias',
  'idRenuncias',
];

const omitUnusedProps = omitCollection(['id', 'personId']);

const processResume = (person) => {
  const { id, resume, ...rest } = person;
  const newResumeObj = {
    ...removeProps(resume, propsToRemove),
    education: omitUnusedProps(findPersonEducations(id)),
    experience: omitUnusedProps(findPersonExperiences(id)),
    waivers: omitUnusedProps(findPersonWaivers(id)),
    trajectory: [],
  };

  return { id, resume: newResumeObj, ...rest };
};

export default processResume;
