import * as R from 'ramda';
import { getObjectFromFile } from '../../../helpers/files';
import { translatePropsCollection } from '../../../helpers/translator';
import { findAllByProp } from '../../../helpers/find';

const propsToTranslate = {
  'idExperiencia': 'id',
  'idPersona': 'personId',
  'centroTrabajo': 'company',
  'idLugarTrabajo': 'placeId',
  'ocupacionProfesion': 'profession',
  'fechaInicio': 'startYear',
  'fechaFin': 'endYear',
};

const raws = getObjectFromFile('experiencia.json');
const experiences = translatePropsCollection(raws, propsToTranslate);

const findPersonExperiences = findAllByProp(experiences, R.__, 'personId');

console.log('loading entities: Resume -> Experience');
export { experiences, findPersonExperiences };
