import * as R from 'ramda';
import { getObjectFromFile } from '../../../helpers/files';
import { translatePropsCollection } from '../../../helpers/translator';
import { findAllByProp } from '../../../helpers/find';

const propsToTranslate = {
  'idEducacion': 'id',
  'idPersona': 'personId',
  'nivel': 'level',
  'concluida': 'completed',
  'centroEstudio': 'institution',
  'detalle': 'details',
};

const raws = getObjectFromFile('educacion.json');
const educations = translatePropsCollection(raws, propsToTranslate);

const findPersonEducations = findAllByProp(educations, R.__, 'personId');

console.log('loading entities: Resume -> Education');
export { educations, findPersonEducations };
