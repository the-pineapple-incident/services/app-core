import * as R from 'ramda';
import { findAllByProp } from '../../helpers/find';
import { getObjectFromFile } from '../../helpers/files';
import { translatePropsCollection } from '../../helpers/translator';
import { removeProps } from '../../helpers/replacement';
import { omitCollection } from 'batch/voto-informado/helpers/omit';

const propsToTranslateCivil = {
  'idSentenciaCivil': 'id',
  'idPersona': 'personId',
  'materiaSentencia': 'sentence',
  'expediente': 'record',
  'organoJuridico': 'judicialAuthority',
  'falloObliga': 'verdict',
};

const propsToTranslateCriminal = {
  'idSentenciaPenal': 'id',
  'idPersona': 'personId',
  'expedientePenal': 'record',
  'fechaSentenciaPenal': 'sentenceDate',
  'organoJudicialPenal': 'judicialAuthority',
  'delitoPenal': 'penalCrime',
  'falloPenal': 'verdict',
  'modalidad': 'modality',
};

const rawsCivil = getObjectFromFile('sentenciaCivil.json');
const rawsCriminal = getObjectFromFile('sentenciaPenal.json');

const civils = translatePropsCollection(rawsCivil, propsToTranslateCivil);
const criminals = translatePropsCollection(rawsCriminal, propsToTranslateCriminal);

const findPersonCivils = findAllByProp(civils, R.__, 'personId');
const findPersonCriminals = findAllByProp(criminals, R.__, 'personId');

const propsToRemove = [
  'idSentenciasPenales',
  'idSentenciasCiviles',
];

const omitUnusedProps = omitCollection(['id', 'personId']);

const processJudgment = (person) => {
  const { id, judgment, ...rest } = person;
  const newJudgmentObj = {
    ...removeProps(judgment, propsToRemove),
    civil: omitUnusedProps(findPersonCivils(id)),
    criminal: omitUnusedProps(findPersonCriminals(id)),
  };

  return { id, judgment: newJudgmentObj, ...rest };
};

console.log('loading entities: Judgment');
export { civils, criminals, findPersonCivils, findPersonCriminals };
export default processJudgment;
