
const createDwelling = ({ dwellingId }) => ({
  place: dwellingId,
  address: null,
});

const replaceDwelling = {
  newPropName: 'dwelling',
  toExtract: ['dwellingId'],
  makeWithExtract: createDwelling,
};

export default replaceDwelling;
