import { isNilOrEmpty } from '../../helpers/conditionals';
import identityType from '../../../../persistence/models/person/identity-type';

const createIdentity = ({ dni, passport }) => ({
  code: !isNilOrEmpty(dni) ? dni : passport,
  type: !isNilOrEmpty(dni) ? identityType.DNI : identityType.PASSPORT,
});

const replaceIdentity = {
  newPropName: 'identity',
  toExtract: ['dni', 'passport'],
  makeWithExtract: createIdentity,
};

export default replaceIdentity;
