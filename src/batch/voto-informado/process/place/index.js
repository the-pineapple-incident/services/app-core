import * as R from 'ramda';
import { getObjectFromFile } from '../../helpers/files';
import { translateProps } from '../../helpers/translator';

// TRANSLATOR
const propsToTranslate = {
  'idLugar': 'id',
  'pais': 'country',
  'departamento': 'department',
  'provincia': 'province',
  'distrito': 'district',
  'codUbigeo': 'code',
};

// LOGIC FOR INSERT EVALUATION
const evaluateToInsert = (place) => {
  if (!!place.district) {
    const able = !!place.province && !!place.department && !!place.country;
    return {
      able,
      type: 'district',
      message: able ? 'ok' : `"district" place with code ${place.code} doesnt have parents`,
    };
  }

  if (!!place.province) {
    const able = !!place.department && !!place.country;
    return {
      able,
      type: 'province',
      message: able ? 'ok' : `"province" place with code ${place.code} doesnt have parents`,
    };
  }

  if (!!place.department) {
    const able = !!place.country;
    return {
      able,
      type: 'department',
      message: able ? 'ok' : `"department" place with code ${place.code} doesnt have parents`,
    };
  }

  const able = !!place.country;
  return {
    able,
    type: 'country',
    message: able ? 'ok' : `"country" place with code ${place.code} is empty`,
  };
};

// EXECUTE
const raws = getObjectFromFile('lugar_no_duplicados.json');

const addTypeAndName = (place) => {
  const { type } = evaluateToInsert(place);
  return {
    ...place,
    type,
    name: R.prop(type, place),
  };
};

const refactor = R.pipe(
  translateProps(R.__, propsToTranslate),
  addTypeAndName,
  R.omit(['department', 'province', 'district', 'country']),
);

const places = R.map(refactor, raws);

console.log('loading entities: Place');
export { places };
