import * as R from 'ramda';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translateProps } from 'batch/voto-informado/helpers/translator';
import { findById as findElementById } from 'batch/voto-informado/process/plan/element';

// {
//   'idPlanGobierno': 0,
//   'idPartidoPolitico': 4,
//   'urlDocumentoCompleto': 'https://declara.jne.gob.pe/ASSETS/PLANGOBIERNO/FILEPLANGOBIERNO/16511.pdf',
//   'resumen': {
//     'ideario': null,
//     'vision': null,
//     'sintesis': {
//       'dimensionSocial': [0],
//       'dimensionEconomica': [1],
//       'dimensionAmbiental': [2],
//       'dimensionInstitucional': [3],
//     },
//     'propuestaRendicionCuentas': null,
//   },
// };

const governmentPlanTranslate = {
  'idPlanGobierno': 'id',
  'idPartidoPolitico': 'politicalPartyId',
  'urlDocumentoCompleto': 'documentUrl',
  'resumen': 'summary',
};

const summaryTranslate = {
  'ideario': 'ideology',
  'vision': 'vision',
  'sintesis': 'dimensions',
  'propuestaRendicionCuentas': 'accountabilityProposal',
};

const dimensionsTranslate = {
  'dimensionSocial': 'social',
  'dimensionEconomica': 'economic',
  'dimensionAmbiental': 'environmental',
  'dimensionInstitucional': 'institutional',
};

const getElements = (elementIds) => R.map(findElementById, elementIds);

const translateDeep = (raw) => {
  const governmentPlan = translateProps(raw, governmentPlanTranslate);
  const summary = translateProps(governmentPlan.summary, summaryTranslate);
  const a = translateProps(summary.dimensions, dimensionsTranslate);
  const { social, economic, environmental, institutional } = a;

  return {
    ...governmentPlan,
    summary: {
      ...summary,
      dimensions: {
        social: getElements(social ?? []),
        economic: getElements(economic ?? []),
        environmental: getElements(environmental ?? []),
        institutional: getElements(institutional ?? []),
      },
    },
  };
};

const raws = getObjectFromFile('government-plan/planGobierno.json');
const governmentPlans = R.map(translateDeep, raws);

console.log('loading entities: Government Plans');
export { governmentPlans };
