import * as R from 'ramda';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';

const propsToTranslate = {
  'idElementoPlan': 'id',
  'problemasIdentificados': 'problemsIdentified',
  'objetivosEstrategicos': 'strategicObjectives',
  'indicadores': 'indicators',
  'metas': 'goals',
  'priorizacion': 'prioritization',
};

const raws = getObjectFromFile('government-plan/elementoPlan.json');
const elements = translatePropsCollection(raws, propsToTranslate);

const indexed = R.indexBy(R.prop('id'), elements);

const findById = (id) => R.compose(R.omit(['id']),  R.prop(id))(indexed);

console.log('loading entities: Government Plan Elements');
export { elements, findById };
