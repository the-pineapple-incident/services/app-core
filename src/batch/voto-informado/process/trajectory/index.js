import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';

const propsToTranslate = {
  'idTrayectoria': 'id',                      // useful to map positions in reverse order
  'idPersona': 'personId',                    // no persist, useful to map trajectories for person
  'idProcesoElectoral': 'politicalEventId',   // no persist, useful for Political Figure
  'idCargo': 'positionId',                    // persist
};

const raws = getObjectFromFile('trayectoria.json');
const trajectories = translatePropsCollection(raws, propsToTranslate);

console.log('loading entities: Trajectories');
export { trajectories };
