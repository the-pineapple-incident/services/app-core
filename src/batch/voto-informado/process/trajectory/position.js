import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translateProps, translatePropsCollection } from 'batch/voto-informado/helpers/translator';
import { replacePropWithOthersCollection } from 'batch/voto-informado/helpers/replacement';
import { castPropCollection } from 'batch/voto-informado/helpers/casting';
import { filterNonPropValues } from 'batch/voto-informado/helpers/filtering';

const propsToTranslate = {
  'idCargoEleccion': 'id',
  'idBidireccional': 'id2',
  'nombre': 'name',
  'tipo': 'type',
  'estadoEleccion': 'electionState',
  'idOrganizacionPolitica': 'politicalPartyId',
  'periodoHistorico': 'period',
};

const raws = getObjectFromFile('cargo.json');
const translated = translatePropsCollection(raws, propsToTranslate);

const periodToTranslate = {
  'anhoInicio': 'startYear',
  'anhoFin': 'endYear',
};

const replacePeriodOps = {
  newPropName: 'period',
  toExtract: 'period',
  makeWithExtract: (period) => (period ? translateProps(period, periodToTranslate) : null),
};

const translationCompleted = replacePropWithOthersCollection(translated, replacePeriodOps);
const filtered = filterNonPropValues('politicalPartyId', translationCompleted);
const positions = castPropCollection('politicalPartyId', Number, filtered);

console.log('loading entities: Positions');
export { positions };
