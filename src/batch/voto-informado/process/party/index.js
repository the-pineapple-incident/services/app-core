import * as R from 'ramda';
import { searchByName } from 'batch/voto-informado/process/party/logo';
import { getObjectFromFile } from '../../helpers/files';
import { translatePropsCollection } from '../../helpers/translator';
import { findExtraInfoParty } from 'batch/voto-informado/process/party/extra-info';

const propsToTranslate = {
  'idPartidoPolitico': 'id',
  'nombre': 'name',
  'descripcion': 'description',
};

const raws = getObjectFromFile('partidoPolitico.json');
const translated = translatePropsCollection(raws, propsToTranslate);

const addLogo = (party) => {
  const logo = searchByName(party.name) ?? null;
  return { ...party, logo };
};

const addExtraInfo = (party) => {
  const info = findExtraInfoParty(party.id) ?? null;
  return {
    ...party,
    ...info,
  };
};

const parties = R.map(R.compose(addLogo, addExtraInfo), translated);

console.log('loading entities: Parties');
export { parties };
