import * as R from 'ramda';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';

const logos = getObjectFromFile('logo/logos.json');
const indexed = R.indexBy(R.prop('name'), logos);

const searchByName = (partyName) => {
  return R.path([partyName, 'logo'], indexed);
};

export { logos, searchByName };
