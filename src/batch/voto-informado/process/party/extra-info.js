import * as R from 'ramda';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translateProps } from 'batch/voto-informado/helpers/translator';

const extraTranslate = {
  'idPartidoPolitico': 'id',
  'nombre': 'name',
  'descripcion': 'description',
};

const descriptionTranslate = {
  'direccion': 'address',
  'telefonos': 'phoneNumbers',
  'paginaWeb': 'webPage',
  'correoContacto': 'contactEmail',
  'correoRegistro': 'contactRegistry',
  'personerosLegales': 'legalSpokesmen',
};

const legalSpokesmenTranslate = {
  'titular': 'titular',
  'alternos': 'alternate',
};

const raws = getObjectFromFile('parties-2021/partidoPoliticoDescripcion.json');

const translateDeep = (raw) => {
  const obj = translateProps(raw, extraTranslate);
  const description = translateProps(obj.description, descriptionTranslate);
  const legalSpokesmen = translateProps(description.legalSpokesmen, legalSpokesmenTranslate);

  return {
    ...obj,
    description: {
      ...description,
      legalSpokesmen
    }
  }
};

const partyDescriptions = R.map(translateDeep, raws);
const indexed = R.indexBy(R.prop('id'), partyDescriptions);
const partyIds2021 = R.keys(indexed);

const findExtraInfoParty = (id) => R.prop(id, indexed) ?? null;

export { partyDescriptions, findExtraInfoParty, partyIds2021 };
