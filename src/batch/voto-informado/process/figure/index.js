import * as R from 'ramda';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { mistakeAnnotations } from 'batch/voto-informado/process/figure/annotations';
import { incomes } from 'batch/voto-informado/process/figure/incomes';
import { immovables, movables, others } from 'batch/voto-informado/process/figure/goods';

const buildGoods = (obj) => ({
  movables: R.path(['bienes', 'idBienesMuebles'], obj),
  immovables: R.path(['bienes', 'idBienesInmuebles'], obj),
  others: R.path(['bienes', 'idBienesOtros'], obj),
});

const buildReported = (obj) => ({
  incomes: R.path(['bienes', 'idIngresos'], obj),
  mistakeAnnotations: R.prop('idAnotacionesMarginales', obj),
  additionalInfo: R.prop('informacionAdicional', obj),
  goods: buildGoods(obj),
});

const buildCandidacy = (obj) => ({
  politicalPartyId: R.path(['datosCandidatura', 'idPartidoPolitico'], obj),
  politicalEventId: R.path(['datosCandidatura', 'idProcesoElectoral'], obj),
  applicationPlaceId: R.path(['datosCandidatura', 'idLugarPostulacion'], obj),
  applicationDate: R.prop('terminoRegistro', obj),
});

const buildPoliticalFigure = (obj) => ({
  id: R.prop('idFiguraPolitica', obj),                                  // no insert
  personId: R.prop('idDatosPersonales', obj),                           // no insert
  positionId: R.path(['datosCandidatura', 'idCargoPostulacion'], obj),  // no insert
  candidacy: buildCandidacy(obj),
  reported: buildReported(obj),
});

const raws = getObjectFromFile('figuraPolitica.json');
const politicalFigures = R.map(buildPoliticalFigure, raws);

console.log('loading entities: Political Figures');
export { politicalFigures, mistakeAnnotations, incomes, immovables, movables, others };
