import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';

const propsToTranslate = {
  'idAnotacionMarginal': 'id',                // no insert
  'idFiguraPolitica': 'politicalFigureId',    // no insert
  'numeroAnotacion': 'annotationNumber',
  'numeroExpediente': 'recordNumber',
  'numeroInforme': 'reportNumber',
  'numeroDocumento': 'documentNumber',
  'contenidoActual': 'currentContent',
  'contenidoCorregido': 'correctedContent',
};

const raws = getObjectFromFile('anotacionMarginal.json');
const mistakeAnnotations = translatePropsCollection(raws, propsToTranslate);

console.log('loading entities: Mistake Annotations');
export { mistakeAnnotations };
