import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';

const propsToTranslate = {
  'idBienInmueble': 'id',
  'idFiguraPolitica': 'politicalFigureId',
  'tipoInmueble': 'type',
  'idUbicacionGeoInmueble': 'placeId',
  'figuraSunarp': 'isInSunarp',
  'partidaSunarp': 'sunarpRecord',
  'autovaluo': 'valuation',
};

const raws = getObjectFromFile('bienInmueble.json');
const immovables = translatePropsCollection(raws, propsToTranslate);

console.log('loading entities: Goods Immovables');
export { immovables };
