import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';

const propsToTranslate = {
  'idBienMueble': 'id',
  'idFiguraPolitica': 'politicalFigureId',      // no insert
  'vehiculo': 'vehicle',
  'marca': 'brand',
  'placa': 'license',
  'anho': 'year',
  'caracteristica': 'characteristics',
  'autovaluo': 'valuation',
};

const raws = getObjectFromFile('bienMueble.json');
const movables = translatePropsCollection(raws, propsToTranslate);

console.log('loading entities: Goods Movables');
export { movables };
