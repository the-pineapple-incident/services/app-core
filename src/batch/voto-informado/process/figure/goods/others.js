import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translatePropsCollection } from 'batch/voto-informado/helpers/translator';

const propsToTranslate = {
  'idBienOtro': 'id',
  'idFiguraPolitica': 'politicalFigureId',
  'nombre': 'name',
  'descripcion': 'description',
  'caracteristica': 'characteristics',
  'valor': 'valuation',
};

const raws = getObjectFromFile('bienOtro.json');
const others = translatePropsCollection(raws, propsToTranslate);

console.log('loading entities: Goods Others');
export { others };
