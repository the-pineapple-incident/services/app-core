import { immovables } from 'batch/voto-informado/process/figure/goods/immovables';
import { movables } from 'batch/voto-informado/process/figure/goods/movables';
import { others } from 'batch/voto-informado/process/figure/goods/others';

export { immovables, movables, others };
