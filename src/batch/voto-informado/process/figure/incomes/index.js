import { __, dissoc, map, pipe, prop } from 'ramda';
import { getObjectFromFile } from 'batch/voto-informado/helpers/files';
import { translateProps } from 'batch/voto-informado/helpers/translator';
import { replacePropWithOthers } from 'batch/voto-informado/helpers/replacement';

const propsToTranslate = {
  'idIngreso': 'id',
  'idFiguraPolitica': 'politicalFigureId',
  'anho': 'year',
  'privado': 'private',
  'publico': 'public',
  'total': 'total',
};

const privatePropsToTranslate = {
  'remBrutaPrivado': 'netIncomes',
  'rentaIndividualPrivado': 'rent',
  'otroIngresoPrivado': 'others',
  'totalPrivado': 'total',
};

const publicPropsToTranslate = {
  'remBrutaPublico': 'netIncomes',
  'rentaIndividualPublico': 'rent',
  'otroIngresoPublico': 'others',
  'totalPublico': 'total',
};

const totalPropsToTranslate = {
  'totalRemBruta': 'netIncomes',
  'totalRenta': 'rent',
  'totalOtro': 'others',
  'total': 'total',
};

const refactorTotal = (obj) => {
  return {
    ...dissoc('total', obj),
    ...prop('total', obj),
  };
};

const replacePrivateProp = {
  newPropName: 'private',
  toExtract: 'private',
  makeWithExtract: (extracted) => translateProps(extracted, privatePropsToTranslate),
};

const replacePublicProp = {
  newPropName: 'public',
  toExtract: 'public',
  makeWithExtract: (extracted) => translateProps(extracted, publicPropsToTranslate),
};

const replaceTotalProp = {
  newPropName: 'total',
  toExtract: 'total',
  makeWithExtract: (extracted) => translateProps(extracted, totalPropsToTranslate),
};

const raws = getObjectFromFile('ingreso.json');

const refactor = pipe(
  translateProps(__, propsToTranslate),
  replacePropWithOthers(__, replacePrivateProp),
  replacePropWithOthers(__, replacePublicProp),
  replacePropWithOthers(__, replaceTotalProp),
  refactorTotal,
);

const incomes = map(refactor, raws);

console.log('loading entities: Incomes');
export { incomes };
