import dbManager from 'persistence/start';
import batchConfig from 'config/batch';

import { places } from 'batch/voto-informado/process/place';
import { events } from 'batch/voto-informado/process/event';
import { positions } from 'batch/voto-informado/process/trajectory/position';
import { people } from 'batch/voto-informado/process/person';
import { parties } from 'batch/voto-informado/process/party';
import { governmentPlans } from 'batch/voto-informado/process/plan';
import { partyIds2021 } from 'batch/voto-informado/process/party/extra-info';
import { trajectories } from 'batch/voto-informado/process/trajectory';
import {
  politicalFigures,
  mistakeAnnotations as annotations,
  movables,
  immovables,
  others,
  incomes,
} from 'batch/voto-informado/process/figure';

import PersistPartiesStamp from 'batch/voto-informado/join/parties';
import PersistPeopleStamp from 'batch/voto-informado/join/people';
import PersistGoodsStamp from 'batch/voto-informado/join/goods';
import PersistIncomesStamp from 'batch/voto-informado/join/incomes';
import PersistPlacesStamp from 'batch/voto-informado/join/place';
import PersistEventsStamp from 'batch/voto-informado/join/events';
import PersistFiguresStamp from 'batch/voto-informado/join/figure';
import PersistAnnotationsStamp from 'batch/voto-informado/join/annotations';
import PersistPositionsStamp from 'batch/voto-informado/join/position';
import PersistTrajectoriesStamp from 'batch/voto-informado/join/trajectories';
import PersistGovernmentPlansStamp from 'batch/voto-informado/join/plan';

import partyRepository from 'persistence/repository/party-repository';
import placeRepository from 'persistence/repository/place-repository';
import personRepository from 'persistence/repository/person-repository';
import positionRepository from 'persistence/repository/position-repository';
import politicalEventRepository from 'persistence/repository/political-event-repository';
import politicalFigureRepository from 'persistence/repository/political-figure-repository';
import governmentPlanRepository from 'persistence/repository/government-plan-repository';

async function persistAll() {
  console.log('persisting: Parties (beginning)');
  const partyManager = PersistPartiesStamp({ parties, partyRepository });
  await partyManager.persist();
  console.log('persisting: Parties (inserted)');

  console.log('persisting: Government Plans (beginning)');
  const planManager = PersistGovernmentPlansStamp({ governmentPlans, governmentPlanRepository });
  await planManager.persist();
  console.log('persisting: Government Plans (inserted)');

  console.log('persisting: Events (beginning)');
  const { currentPresidentialEventID, currentCongresalEventID } = batchConfig.votoInformado.elections;
  const eventManager = PersistEventsStamp({ events, politicalEventRepository, partyManager });
  eventManager.registerParticipantsInCurrentEvent(currentPresidentialEventID, partyIds2021, planManager);
  eventManager.registerParticipantsInCurrentEvent(currentCongresalEventID, partyIds2021, planManager);
  await eventManager.persist();
  console.log('persisting: Events (inserted)');

  console.log('persisting: Places (beginning)');
  const placeManager = PersistPlacesStamp({ places, placeRepository });
  await placeManager.persist();
  console.log('persisting: Places (inserted)');

  // complex insertion: Figures, Positions & Trajectories
  console.log('persisting: Political Figure (beginning)');
  console.log('persisting: Positions (beginning)');

  const incomesManager = PersistIncomesStamp({ incomes });
  const goodsManager = PersistGoodsStamp({ movables, immovables, others });
  const annotationsManager = PersistAnnotationsStamp({ annotations });

  incomesManager.buildIncomes();
  goodsManager.buildGoods();
  annotationsManager.buildAnnotations();

  const dependencies = { partyManager, eventManager, placeManager };
  const figureManager = PersistFiguresStamp({ politicalFigures, goodsManager, incomesManager, annotationsManager, politicalFigureRepository, dependencies });

  const positionManager = PersistPositionsStamp({ positions, positionRepository, partyManager, figureManager });
  const { figures: additionalFigures, trajectories: additionalTrajectories } = positionManager.generateMissedObjects();
  await positionManager.persist();

  figureManager.considerAdditionalFigures(additionalFigures);
  figureManager.buildFigures();

  await figureManager.persist();
  console.log('persisting: Political Figure (inserted)');
  console.log('persisting: Positions (inserted)');

  console.log('persisting: People (beginning)');
  console.log('persisting: Trajectory (beginning)');
  const trajectoryManager = PersistTrajectoriesStamp({ trajectories, positionManager, figureManager });
  trajectoryManager.considerAdditionalTrajectories(additionalTrajectories);
  trajectoryManager.buildTrajectories();
  console.log('persisting: Trajectory (loaded)');

  const personManager = PersistPeopleStamp({ people, personRepository, placeManager, trajectoryManager, partyManager });
  await personManager.persist();
  console.log('persisting: People (inserted)');

  // console.log('persisting more: Political Figures (person ids)');
  // await figureManager.addPersonIdsToAllFigures(personManager);
  // console.log('end!');
}

async function execute() {
  await dbManager.startConnection();
  await persistAll();
}

execute().catch((error) => console.log(error));
