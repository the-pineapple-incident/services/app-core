#!/bin/bash

### 1. change file extension for json
pushd data/parties-2021/scraping || exit
for file in *.ndjson; do
  cp -- "$file" "../${file%.ndjson}.json"
done
popd || exit

### 2. convert content to valid json
pushd data/parties-2021 || exit
for file in *.json; do
  sed -i '$!s/$/,/' "$file"           # commas except last line
  sed -i '1s/^/[/;$s/$/]/' "$file"    # curly braces at begin and end []
done
popd || exit

# https://stackoverflow.com/questions/35021524/how-can-i-add-a-comma-at-the-end-of-every-line-except-the-last-line
