
const placeType = Object.freeze({
  DEPARTMENT: 'department',
  PROVINCE: 'province',
  DISTRICT: 'district',
  COUNTRY: 'country',
});

export default placeType;
