import mongoose from 'mongoose';
import placeType from './type';

const { Schema } = mongoose;

const PlaceSchema = new Schema({
  code: String,
  type: {
    type: String,
    enum: Object.values(placeType),
  },
  parentId: Schema.Types.ObjectId,
  name: String,
});

const Place = mongoose.model('Place', PlaceSchema);

export default Place;
