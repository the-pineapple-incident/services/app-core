import mongoose from 'mongoose';

const { Schema } = mongoose;

const ElementSchema = new Schema({
  problemsIdentified: String,
  strategicObjectives: String,
  indicators: String,
  goals: String,
  prioritization: String,
}, { _id: false });

export default ElementSchema;
