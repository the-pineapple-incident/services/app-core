import mongoose from 'mongoose';

import ElementSchema from './element';

const { Schema } = mongoose;

const GovernmentPlanSchema = new Schema({
  documentUrl: String,
  summary: {
    ideology: String,
    vision: String,
    accountabilityProposal: String,
    dimensions: {
      social: [ElementSchema],
      economic: [ElementSchema],
      environmental: [ElementSchema],
      institutional: [ElementSchema],
    },
  },
});

const GovernmentPlan = mongoose.model('GovernmentPlan', GovernmentPlanSchema);

export default GovernmentPlan;
