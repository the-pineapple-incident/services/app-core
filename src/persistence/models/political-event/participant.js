import mongoose from 'mongoose';

const { Schema } = mongoose;

const ParticipantSchema = new Schema({
  politicalPartyId: {
    type: Schema.Types.ObjectId,
    ref: 'PoliticalParty',
  },
  governmentPlanId: {
    type: Schema.Types.ObjectId,
    ref: 'GovernmentPlan',
  },
}, { _id: false });

export default ParticipantSchema;
