const eventState = Object.freeze({
  ACTIVE: 'ACTIVE',
  FINISHED: 'FINISHED',
});

export default eventState;
