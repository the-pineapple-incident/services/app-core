import mongoose from 'mongoose';
import eventState from './event-state';
import ParticipantSchema from './participant';

const { Schema } = mongoose;

const PoliticalEventSchema = new Schema({
  name: String,
  state: {
    type: String,
    enum: Object.values(eventState),
  },
  year: String,
  participants: [ParticipantSchema],
});

const PoliticalEvent = mongoose.model('PoliticalEvent', PoliticalEventSchema);

export default PoliticalEvent;
