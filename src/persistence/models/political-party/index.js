import mongoose from 'mongoose';

const { Schema } = mongoose;

const PoliticalPartySchema = new Schema({
  name: String,
  logo: String,
  description: {
    address: String,
    phoneNumbers: [String],
    webPage: String,
    contactEmail: String,
    contactRegistry: String,
    legalSpokesmen: {
      titular: String,
      alternate: [String],
    },
  },
});

const PoliticalParty = mongoose.model('PoliticalParty', PoliticalPartySchema);

export default PoliticalParty;
