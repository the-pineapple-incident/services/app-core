import mongoose from 'mongoose';
import Topic from './topic';

const { Schema } = mongoose;

const DimensionSchema = new Schema({
  name: String,
  description: String,
  topics: [Topic],
});

const Dimension = mongoose.model('Dimension', DimensionSchema);

export default Dimension;
