import mongoose from 'mongoose';

const { Schema } = mongoose;

const ThemeSchema = new Schema({
  name: String,
  description: String,
  keywords: [String],
  sources: [String],
  opinions: [{
    type: Schema.Types.ObjectId,
    ref: 'Opinion',
  }],
}, { _id: false });

export default ThemeSchema;
