import mongoose from 'mongoose';
import Theme from './theme';

const { Schema } = mongoose;

const TopicSchema = new Schema({
  name: String,
  description: String,
  themes: [Theme],
}, { _id: false });

export default TopicSchema;
