import mongoose from 'mongoose';
import opinionType from './opinion-type';

const { Schema } = mongoose;

const OpinionSchema = new Schema({
  quote: String,
  source: String,
  date: Date,
  stance: String,
  politicFigure: {
    type: Schema.Types.ObjectId,
    ref: 'PoliticalFigure',
  },
  opinionType: {
    type: String,
    enum: Object.values(opinionType),
  },
});

const Opinion = mongoose.model('Opinion', OpinionSchema);

export default Opinion;
