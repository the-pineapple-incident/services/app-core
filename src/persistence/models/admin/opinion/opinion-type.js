const opinionType = Object.freeze({
  EVENT: 'EVENT',
  THEME: 'THEME',
});

export default opinionType;
