import mongoose from 'mongoose';

const { Schema } = mongoose;

const EventSchema = new Schema({
  name: String,
  description: String,
  period: {
    startDate: Date,
    endDate: Date,
  },
  keywords: [String],
  opinions: [{
    type: Schema.Types.ObjectId,
    ref: 'Opinion',
  }],
  isActive: Boolean,
  friendlyId: String,
});

const Event = mongoose.model('Event', EventSchema);

export default Event;
