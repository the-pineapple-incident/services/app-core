import mongoose from 'mongoose';

const { Schema } = mongoose;

const CivilSchema = new Schema({
  sentence: String,
  record: String,
  judicialAuthority: String,
  verdict: String,
}, { _id: false });

export default CivilSchema;
