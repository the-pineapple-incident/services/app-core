import mongoose from 'mongoose';

const { Schema } = mongoose;

const CriminalSchema = new Schema({
  record: String,
  sentenceDate: Date,
  judicialAuthority: String,
  penalCrime: String,
  verdict: String,
  modality: String,
}, { _id: false });

export default CriminalSchema;
