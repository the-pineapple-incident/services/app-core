const identityType = Object.freeze({
  DNI: 'DNI',
  PASSPORT: 'PASSPORT',
});

export default identityType;
