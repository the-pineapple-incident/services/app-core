import mongoose from 'mongoose';

const { Schema } = mongoose;

const ExperienceSchema = new Schema({
  company: String,
  place: {
    type: Schema.Types.ObjectId,
    ref: 'Place',
  },
  profession: String,
  startYear: String,
  endYear: String,
}, { _id: false });

export default ExperienceSchema;
