const electionState = Object.freeze({
  ELECTED: 'ELEGIDO',
  NO_ELECTED: 'NO_ELEGIDO',
  IN_PROCESS: 'EN_PROCESO',
});

export default electionState;
