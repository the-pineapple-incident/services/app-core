import mongoose from 'mongoose';

import positionType from './position-type';
import electionState from './election-state';

const { Schema } = mongoose;

const PositionSchema = new Schema({
  name: String,
  type: {
    type: String,
    enum: Object.values(positionType),
  },
  electionState: {
    type: String,
    enum: Object.values(electionState).concat([null]),
  },
  politicalPartyId: {
    type: Schema.Types.ObjectId,
    ref: 'PoliticalParty',
  },
  period: {
    startYear: String,
    endYear: String,
  },
});

const Position = mongoose.model('Position', PositionSchema);

export default Position;
