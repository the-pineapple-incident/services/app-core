const positionType = Object.freeze({
  CANDIDATE: 'CARGO_POSTULACION',
  PARTY: 'CARGO_PARTIDARIO',
});

export default positionType;
