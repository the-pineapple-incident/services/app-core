import mongoose from 'mongoose';

const { Schema } = mongoose;

const TrajectorySchema = new Schema({
  positionId: {
    type: Schema.Types.ObjectId,
    ref: 'Position',
    required: false,
  },
  politicalFigureId: {
    type: Schema.Types.ObjectId,
    ref: 'PoliticalFigure',
    required: false,
  },
}, { _id: false });

export default TrajectorySchema;
