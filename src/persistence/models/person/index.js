import mongoose from 'mongoose';
import identityType from './identity-type';
import Civil from './judgment/civil';
import Criminal from './judgment/criminal';
import Education from './education';
import Experience from './experience';
import Trajectory from './trajectory';
import Waiver from './waiver';

const { Schema } = mongoose;

const PersonSchema = new Schema({
  name: String,
  firstLastname: String,
  secondLastname: String,
  gender: String,
  profilePicture: String,
  birth: {
    date: Date,
    place: {
      type: Schema.Types.ObjectId,
      ref: 'Place',
    },
  },
  dwelling: {
    address: String,
    place: {
      type: Schema.Types.ObjectId,
      ref: 'Place',
    },
  },
  identity: {
    type: {
      type: String,
      enum: Object.values(identityType),
    },
    code: String,
  },
  judgment: {
    civil: [Civil],
    criminal: [Criminal],
  },
  resume: {
    education: [Education],
    experience: [Experience],
    trajectory: [Trajectory],
    waivers: [Waiver],
  },
});

const Person = mongoose.model('Person', PersonSchema);

export default Person;
