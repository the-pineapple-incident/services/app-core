import mongoose from 'mongoose';

const { Schema } = mongoose;

const WaiverSchema = new Schema({
  politicalPartyId: {
    type: Schema.Types.ObjectId,
    ref: 'PoliticalParty',
  },
  date: Date,
}, { _id: false });

export default WaiverSchema;
