import mongoose from 'mongoose';
import educationLevel from './education-level';

const { Schema } = mongoose;

const EducationSchema = new Schema({
  level: {
    type: String,
    enum: Object.values(educationLevel),
  },
  completed: Boolean,
  institution: String,
  details: {
    specialty: String,
    bachelorYear: String,
    graduateYear: String,
    degree: String,
  },
}, { _id: false });

export default EducationSchema;
