import mongoose from 'mongoose';

const { Schema } = mongoose;

const IncomesSchema = new Schema({
  year: String,
  private: {
    netIncomes: Number,
    rent: Number,
    others: Number,
    total: Number,
  },
  public: {
    netIncomes: Number,
    rent: Number,
    others: Number,
    total: Number,
  },
  netIncomes: Number,
  rent: Number,
  others: Number,
  total: Number,
}, { _id: false });

export default IncomesSchema;
