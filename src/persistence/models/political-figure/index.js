import mongoose from 'mongoose';

import IncomesSchema from './incomes';
import MistakeAnnotationSchema from './mistake-annotation';
import ImmovableSchema from './goods/inmovable';
import MovableSchema from './goods/movable';
import OtherSchema from './goods/other';

const { Schema } = mongoose;

const PoliticalFigureSchema = new Schema({
  candidacy: {
    politicalPartyId: {
      type: Schema.Types.ObjectId,
      ref: 'PoliticalParty',
    },
    politicalEventId: {
      type: Schema.Types.ObjectId,
      ref: 'PoliticalEvent',
    },
    applicationPlaceId: {
      type: Schema.Types.ObjectId,
      ref: 'Place',
    },
    applicationDate: Date,
  },
  reported: {
    incomes: IncomesSchema,
    mistakeAnnotations: [MistakeAnnotationSchema],
    additionalInfo: [String],
    goods: {
      movables: [MovableSchema],
      immovables: [ImmovableSchema],
      others: [OtherSchema],
    },
  },
});

const PoliticalFigure = mongoose.model('PoliticalFigure', PoliticalFigureSchema);

export default PoliticalFigure;
