import mongoose from 'mongoose';

const { Schema } = mongoose;

const MistakeAnnotationSchema = new Schema({
  annotationNumber: String,
  recordNumber: String,
  reportNumber: String,
  documentNumber: String,
  currentContent: String,
  correctedContent: String,
}, { _id: false });

export default MistakeAnnotationSchema;
