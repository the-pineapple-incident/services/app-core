import mongoose from 'mongoose';

const { Schema } = mongoose;

const OtherSchema = new Schema({
  name: String,
  description: String,
  characteristics: String,
  valuation: Number,
}, { _id: false });

export default OtherSchema;
