import mongoose from 'mongoose';

const { Schema } = mongoose;

const MovableSchema = new Schema({
  vehicle: String,
  brand: String,
  license: String,
  model: String,
  year: String,
  characteristics: String,
  valuation: Number,
}, { _id: false });

export default MovableSchema;
