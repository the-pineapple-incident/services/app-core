import mongoose from 'mongoose';

const { Schema } = mongoose;

const ImmovableSchema = new Schema({
  type: String,
  placeId: {
    type: Schema.Types.ObjectId,
    ref: 'Place',
  },
  isInSunarp: Boolean,
  sunarpRecord: String,
  valuation: Number,
}, { _id: false });

export default ImmovableSchema;
