import Topic from '../models/admin/dimension/topic';

const addThemeToTopic = async (topicName, theme) => {
  await Topic.updateOne({ name: topicName }, { $push: { themes: theme } });
};

const topicRepository = {
  addThemeToTopic,
};

export default topicRepository;
