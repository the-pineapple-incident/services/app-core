import Position from 'persistence/models/person/trajectory/position';

const insertAll = async (positions) => {
  return await Position.insertMany(positions);
};

const positionRepository = {
  insertAll,
};

export default positionRepository;
