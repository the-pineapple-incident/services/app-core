import PoliticalFigure from '../models/political-figure';

const getAll = async () => await PoliticalFigure.find();

const getAllOfEvent = async (idPoliticalEvent) => {
  return await PoliticalFigure.find({ 'candidacy.politicalEventId': idPoliticalEvent }).exec();
};

const insertAll = async (figures) => {
  return await PoliticalFigure.insertMany(figures);
};

const updateAll = async (figures) => {
  return PoliticalFigure.updateMany({}, figures);
};

const findByPartyIdAndEventId = async (politicalPartyId, politicalEventId) => {
  return await PoliticalFigure.find({
    'candidacy.politicalPartyId': politicalPartyId,
    'candidacy.politicalEventId': politicalEventId,
  }).exec();
};

const findById = async (id) => {
  return await PoliticalFigure
    .findOne({ _id: id })
    .exec();
};

const politicalFigureRepository = {
  getAll,
  updateAll,
  getAllOfEvent,
  insertAll,
  findByPartyIdAndEventId,
  findById,
};

export default politicalFigureRepository;
