import Place from '../models/place';

const insertAll = async (places) => {
  return await Place.insertMany(places);
};

const findById = async (id) => {
  return await Place.findById(id).exec();
};

const placeRepository = {
  insertAll,
  findById,
};

export default placeRepository;
