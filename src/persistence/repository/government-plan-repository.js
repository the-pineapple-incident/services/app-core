import GovernmentPlan from 'persistence/models/government-plan';

const insertAll = async (plans) => {
  return await GovernmentPlan.insertMany(plans);
};

const findAllByIds = async (ids) => {
  return await GovernmentPlan
    .find()
    .where('_id').in(ids)
    .exec();
};

const governmentPlanRepository = {
  insertAll,
  findAllByIds,
};

export default governmentPlanRepository;
