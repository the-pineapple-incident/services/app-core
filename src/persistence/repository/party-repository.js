import PoliticalParty from '../models/political-party';

const getAll = async () => {
  return await PoliticalParty.find().exec();
};

const findById = async (id) => {
  return await PoliticalParty.findById(id).exec();
};

const findAllByIds = async (ids) => {
  return await PoliticalParty
    .find()
    .where('_id').in(ids)
    .exec();
};

const insert = async (party) => {
  return await PoliticalParty.create(party);
};

const insertAll = async (parties) => {
  return await PoliticalParty.insertMany(parties);
};

const updateAll = async (parties) => {
  await PoliticalParty.updateMany(parties);
};

const getIdsByNames = async (names) => {
  return await PoliticalParty
    .find()
    .where('name').in(names)
    .select('name')
    .exec();
};

const getAllByPoliticalEvent = async (id) => {
  return await PoliticalParty.find({ politicalEvent: id }).exec();
};

const updateLogo = async ({
  name,
  logo,
}) => {
  return (await PoliticalParty.updateOne({ 'name': name }, { $set: { 'logo': logo } }).exec())._id;
};

const partyRepository = {
  insert,
  insertAll,
  updateAll,
  getAll,
  findById,
  findAllByIds,
  getIdsByNames,
  getAllByPoliticalEvent,
  updateLogo,
};

export default partyRepository;
