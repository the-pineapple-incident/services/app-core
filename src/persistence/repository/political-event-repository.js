import PoliticalEvent from '../models/political-event';
import eventState from '../models/political-event/event-state';

const getAllActive = async () => {
  return await PoliticalEvent
    .find({ state: eventState.ACTIVE })
    .exec();
};

const insertAll = async (events) => {
  return await PoliticalEvent.insertMany(events);
};

const findById = async (id) => {
  return await PoliticalEvent.findById(id).exec();
};

const politicalEventRepository = {
  getAllActive,
  insertAll,
  findById,
};

export default politicalEventRepository;
