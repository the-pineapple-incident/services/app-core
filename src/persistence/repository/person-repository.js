import Person from '../models/person';

const insertAll = async (people) => {
  return await Person.insertMany(people);
};

const findPoliticalFigureById = async (id) => {
  return await Person.findOne({ 'resume.trajectory': { $elemMatch: { politicalFigureId: id } } }).exec();
};

const findFigureId = async (dni) => {
  const person = await Person.findOne({ 'identity.code': dni }).exec();
  const { trajectory } = person.resume;
  let politicFigureId = null;
  trajectory.forEach((elem) => {
    if (elem.politicalFigureId) politicFigureId = elem.politicalFigureId;
  });

  return politicFigureId;
};

const personRepository = {
  insertAll,
  findPoliticalFigureById,
  findFigureId,
};

export default personRepository;
