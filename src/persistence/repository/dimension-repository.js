import Dimension from '../models/admin/dimension';
import Opinion from '../models/admin/opinion';

const insert = async (dimension) => {
  return (await Dimension.create(dimension))._id;
};

const addTopicToDimension = async (id, topic) => {
  await Dimension.updateOne({ _id: id }, { $push: { topics: topic } });
};

const findById = async (id) => {
  return await  Dimension
    .findOne({ _id: id })
    .exec();
};

const addThemeToTopic = async (idDimension, topicName, theme) => {
  await Dimension.updateOne(
    { _id: idDimension, topics: { $elemMatch: { name: topicName } } },
    { $push: { 'topics.$.themes': theme } },
  );
};

const addOpinionToTheme = async (idDimension, topicName, themeName, idOpinion) => {
  await Dimension.updateOne(
    {},
    { $push: { 'topics.$[i].themes.$[j].opinions': idOpinion } },
    { arrayFilters: [{ 'i.name': topicName }, { 'j.name': themeName }] },
  );
};

const findAll = async () => {
  return await Dimension.find().exec();
};

const getAllTopicsOfDimension = async (id) => {
  const dimension = await Dimension.findById(id).exec();

  if (dimension === null) return [];
  return dimension.topics;
};

const getAllThemesOfTopic = async (topicName) => {
  const dimension = await Dimension.findOne({ topics: { $elemMatch: { name: topicName } } });

  if (dimension === null) return [];
  return dimension.topics[0].themes;
};

const getAllOpinionsOfTheme = async (themeName) => {
  const dimension = await Dimension
    .findOne({ 'topics.themes': { $elemMatch: { name: themeName } } })
    .populate({ path: 'topics.themes.opinions', model: Opinion });

  if (dimension === null) return [];
  return dimension.topics[0].themes[0].opinions;
};

const dimensionRepository = {
  insert,
  addTopicToDimension,
  findById,
  addThemeToTopic,
  addOpinionToTheme,
  findAll,
  getAllTopicsOfDimension,
  getAllThemesOfTopic,
  getAllOpinionsOfTheme,
};

export default dimensionRepository;
