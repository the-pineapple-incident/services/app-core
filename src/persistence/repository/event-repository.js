import Event from '../models/admin/event';
import Opinion from '../models/admin/opinion';

const insert = async (event) => {
  return (await Event.create(event))._id;
};

const addOpinionToEvent = async (id, idOpinion) => {
  await Event.updateOne({ _id: id }, { $push: { opinions: idOpinion } });
};

const getAllOpinionsOfEvent = async (id) => {
  const res = await Event
    .findOne({ _id: id })
    .populate({ path: 'opinions', model: Opinion })
    .exec();

  return res.opinions;
};

const findAllActive = async () => {
  return await Event.find({ isActive: true }).exec();
};

const findByNameAndSave = async (event) => {
  const eventSaved = await Event.find({ name: eventName }).exec();
  if (eventSaved !== undefined) return eventSaved._id;
  return await insert(event);
};

const eventRepository = {
  insert,
  addOpinionToEvent,
  getAllOpinionsOfEvent,
  findAllActive,
  findByNameAndSave,
};

export default eventRepository;
