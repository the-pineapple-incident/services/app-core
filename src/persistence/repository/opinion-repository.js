import Opinion from '../models/admin/opinion';

const insert = async (opinion) => {
  return (await Opinion.create(opinion))._id;
};

const findOpinionsOfPoliticFigure = async (idPoliticFigure) => {
  return await Opinion.find({ politicFigure: idPoliticFigure }).exec();
};

const opinionRepository = {
  insert,
  findOpinionsOfPoliticFigure,
};

export default opinionRepository;
