import dimensionRepository from './dimension-repository';
import eventRepository from './event-repository';
import governmentPlanRepository from './government-plan-repository';
import opinionRepository from './opinion-repository';
import partyRepository from './party-repository';

import personRepository from './person-repository';
import placeRepository from './place-repository';

import politicalEventRepository from './political-event-repository';
import politicalFigureRepository from './political-figure-repository';

import positionRepository from './position-repository';
import topicRepository from './topic-repository';

const repositories = {
  dimensionRepository,
  eventRepository,
  governmentPlanRepository,
  opinionRepository,
  partyRepository,
  personRepository,
  placeRepository,
  politicalEventRepository,
  politicalFigureRepository,
  positionRepository,
  topicRepository,
};

export default repositories;
