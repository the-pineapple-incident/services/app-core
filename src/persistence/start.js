
import mongoose from 'mongoose';
import config from '../config';

const dbManager = {

  async startConnection() {
    try {
      console.log('Connecting to database');
      const { name, host, username, password } = config.database;
      const dbURL = `mongodb+srv://${username}:${password}@${host}/${name}`;

      await mongoose.connect(dbURL, { useNewUrlParser: true, useUnifiedTopology: true });
      console.log('Database connection successful');

    } catch (error) {
      console.error('Error connecting to database', error);
    }
  },
};

export default dbManager;
