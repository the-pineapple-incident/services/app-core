
async function getPersonOf({ personRepository }, { id }) {
  const person = await personRepository.findPoliticalFigureById(id);
  console.log(person._doc._id);
  // console.log(person._doc);
  return person;
}

export default getPersonOf;
