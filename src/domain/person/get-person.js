
async function getPerson({ personRepository }, { personId }) {
  return await personRepository.findById(personId);
}

export default getPerson;
