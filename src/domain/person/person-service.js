import injectDependencies from '../injection';
import getPerson from './get-person';
import getPersonOf from './get-person-of';

const personService = {
  getPerson: injectDependencies(getPerson),
  getPersonOf: injectDependencies(getPersonOf),
};

export default personService;

