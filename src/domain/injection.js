import * as R from 'ramda';
import repositories from '../persistence/repository';

function injectDependency(dependency, fn) {
  // inject dependency indirectly using lambda expression when there's only one argument left
  if (fn.length === 1) {
    return () => fn(dependency);
  }

  return R.curry(fn)(dependency);
}

// R.curry gets f(n1, n2, ...) and repositories as n1
// then returns g(n2, ...) === f(repositories, n2, ...)
export default R.curry(injectDependency)(repositories);
