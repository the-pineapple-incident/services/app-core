import injectDependencies from '../injection';
import getAllPoliticalFigures from './get-all-political-figures';
import getAllPoliticalFiguresOf from './get-all-political-figures-of';
import getPoliticalFigure from './get-political-figure';
import getAllPoliticalFiguresOfEvent from './get-all-politic-figures-of-event';
import getPartyOfFigure from './get-party-of-figure';
import getEventOfFigure from './get-event-of-figure';
import getPlaceOfFigure from './get-place-of-figure';
import getApplicationDate from './get-application-date-of-figure';

const politicalFigureService = {
  getAllPoliticalFigures: injectDependencies(getAllPoliticalFigures),
  getAllPoliticalFiguresOf: injectDependencies(getAllPoliticalFiguresOf),
  getPoliticalFigure: injectDependencies(getPoliticalFigure),
  getAllPoliticalFiguresOfEvent: injectDependencies(getAllPoliticalFiguresOfEvent),
  getPartyOfFigure: injectDependencies(getPartyOfFigure),
  getEventOfFigure: injectDependencies(getEventOfFigure),
  getPlaceOfFigure: injectDependencies(getPlaceOfFigure),
  getApplicationDate: injectDependencies(getApplicationDate),
};

export default politicalFigureService;

