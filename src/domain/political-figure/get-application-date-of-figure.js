async function getApplicationDate({ politicalFigureRepository }, { id }) {
  const figure = await politicalFigureRepository.findById(id);
  return figure.candidacy.applicationDate;
}

export default getApplicationDate;
