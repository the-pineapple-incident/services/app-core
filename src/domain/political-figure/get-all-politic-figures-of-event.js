async function getAllPoliticalFiguresOfEvent({ politicalFigureRepository }, politicalEvent) {
  return await politicalFigureRepository.getAllOfEvent(politicalEvent._id);
}

export default getAllPoliticalFiguresOfEvent;
