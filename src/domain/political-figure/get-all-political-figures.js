
async function getAllPoliticalFigures({ politicalFigureRepository }) {
  return await politicalFigureRepository.getAll();
}

export default getAllPoliticalFigures;
