async function getAllPoliticalFiguresOf({ politicalFigureRepository }, politicalPartyId, politicalEventId) {
  // const findPerson = async (politicalFigure) => ({ ...politicalFigure, ...await personService.getPersonOf(politicalFigure._id) });
  // return R.map(findPerson, politicalFigures);
  return await politicalFigureRepository.findByPartyIdAndEventId(politicalPartyId, politicalEventId);
}

export default getAllPoliticalFiguresOf;
