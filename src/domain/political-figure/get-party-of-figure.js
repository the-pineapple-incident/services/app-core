async function getPartyOfFigure({ partyRepository }, politicalPartyId) {
  return await partyRepository.findById(politicalPartyId);
}

export default getPartyOfFigure;
