async function getEventOfFigure({ politicalEventRepository }, politicalEventId) {
  return await politicalEventRepository.findById(politicalEventId);
}

export default getEventOfFigure;
