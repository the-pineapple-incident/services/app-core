async function getPlaceOfFigure({ placeRepository }, placeId) {
  return await placeRepository.findById(placeId);
}

export default getPlaceOfFigure;
