async function getPoliticalFigure({ politicalFigureRepository }, politicalFigureId) {
  // politicalFigure.person = await personService.getPerson(personId);
  return await politicalFigureRepository.findById(politicalFigureId);
}

export default getPoliticalFigure;
