
// Use Case
async function findPartyByID({ partyRepository }, { id }) {
  return await partyRepository.findById(id);
}

export default findPartyByID;
