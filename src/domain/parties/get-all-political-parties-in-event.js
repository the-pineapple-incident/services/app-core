
async function getAllPartiesInEvent({ partyRepository }, { id }) {
  return await partyRepository.getAllByPoliticalEvent(id);
}

export default getAllPartiesInEvent;
