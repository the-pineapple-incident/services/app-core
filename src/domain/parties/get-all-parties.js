
async function getAllParties({ partyRepository }) {
  return await partyRepository.getAll();
}

export default getAllParties;
