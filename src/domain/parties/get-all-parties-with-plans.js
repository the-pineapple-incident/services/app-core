import * as R from 'ramda';

export const buildParticipant = R.curry((parties, plans, participant) => {
  const { politicalPartyId, governmentPlanId } = participant;

  return {
    politicalParty: R.prop(politicalPartyId, parties) ?? null,
    governmentPlan: R.prop(governmentPlanId, plans) ?? null,
  };
});

async function getAllPartiesWithPlans({ partyRepository, governmentPlanRepository }, participants) {
  const partyIds = R.pluck('politicalPartyId', participants);
  const planIds = R.pluck('governmentPlanId', participants);

  const [parties, plans] = await Promise.all([
    partyRepository.findAllByIds(partyIds),
    governmentPlanRepository.findAllByIds(planIds),
  ]);

  const partiesIndexed = R.indexBy(R.prop('_id'), parties);
  const plansIndexed = R.indexBy(R.prop('_id'), plans);

  return R.map(buildParticipant(partiesIndexed, plansIndexed), participants);
}

export default getAllPartiesWithPlans;
