import injectDependencies from '../injection';
import getAllParties from './get-all-parties';
import findPartyByID from './find-party';
import getAllPartiesInEvent from './get-all-political-parties-in-event';
import getAllPartiesById from './get-all-parties-by-id';
import getAllPartiesWithPlans from './get-all-parties-with-plans';

const partyService = {
  getAllParties: injectDependencies(getAllParties),
  findPartyByID: injectDependencies(findPartyByID),
  getAllPartiesInEvent: injectDependencies(getAllPartiesInEvent),
  getAllPartiesById: injectDependencies(getAllPartiesById),
  getAllPartiesWithPlans: injectDependencies(getAllPartiesWithPlans),
};

export default partyService;
