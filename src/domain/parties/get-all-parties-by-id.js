
async function getAllPartiesById({ partyRepository }, partyIds) {
  return await partyRepository.findAllByIds(partyIds);
}

export default getAllPartiesById;
