import { politicalParties } from '../../scraping/extra/process/political-party/get-logos';

async function saveAllLogos({ partyRepository }) {

  const partiesUpdatePromises = [];

  politicalParties.forEach((party) => {
    partiesUpdatePromises.push(partyRepository.updateLogo(party));
  });

  await Promise.all(
    partiesUpdatePromises,
  );
}

export default saveAllLogos;
