import { isNil, pluck } from 'ramda';

async function getParticipatingPartiesOfEvent({ politicalEventRepository, partyRepository }, eventId) {
  const currentEvent = await politicalEventRepository.findById(eventId);

  if (isNil(currentEvent)) {
    console.log(`Cannot get participating parties of event ${eventId} because doesn't exist`);
    return null;
  }

  const partyIds = pluck('politicalPartyId', currentEvent.participants);
  return partyRepository.findAllByIds(partyIds);
}

export default getParticipatingPartiesOfEvent;
