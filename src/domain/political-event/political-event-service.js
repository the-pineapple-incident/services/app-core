import injectDependencies from '../injection';
import getAllActivePoliticalEvents from './get-all-active';
import getParticipatingPartiesOfEvent from './get-participating-parties';

const politicalEventService = {
  getAllActivePoliticalEvents: injectDependencies(getAllActivePoliticalEvents),
  getParticipatingPartiesOfEvent: injectDependencies(getParticipatingPartiesOfEvent),
};

export default politicalEventService;
