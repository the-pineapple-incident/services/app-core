async function getAllActivePoliticalEvents({ politicalEventRepository }) {
  return await politicalEventRepository.getAllActive();
}

export default getAllActivePoliticalEvents;
