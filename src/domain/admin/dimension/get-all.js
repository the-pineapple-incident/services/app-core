async function getAllDimensions({ dimensionRepository }) {
  return await dimensionRepository.findAll();
}

export default getAllDimensions;
