async function addDimension({ dimensionRepository }, dimension) {
  return await dimensionRepository.insert(dimension);
}

export default addDimension;
