import addDimension from './add-dimension';
import getAllDimensions from './get-all';
import injectDependencies from '../../injection';

const dimensionService = {
  addDimension: injectDependencies(addDimension),
  findAll: injectDependencies(getAllDimensions),
};

export default dimensionService;
