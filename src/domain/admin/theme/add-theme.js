async function addTheme({ dimensionRepository }, idDimension, topicName, theme) {
  await dimensionRepository.addThemeToTopic(idDimension, topicName, theme);
}

export default addTheme;
