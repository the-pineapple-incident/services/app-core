import addTheme from './add-theme';
import injectDependencies from '../../injection';
import addOpinionToTheme from './add-opinion';
import getThemesOfTopic from './get-themes-of-topic';

const themeService = {
  addTheme: injectDependencies(addTheme),
  addOpinionToTheme: injectDependencies(addOpinionToTheme),
  getThemesOfTopic: injectDependencies(getThemesOfTopic),
};

export default themeService;
