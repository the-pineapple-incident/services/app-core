async function getThemesOfTopic({ dimensionRepository }, topicName) {
  return await dimensionRepository.getAllThemesOfTopic(topicName);
}

export default getThemesOfTopic;
