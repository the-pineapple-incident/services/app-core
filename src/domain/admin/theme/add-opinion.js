import opinionType from '../../../persistence/models/admin/opinion/opinion-type';

async function addOpinionToTheme({ dimensionRepository, opinionRepository }, opinion, idDimension, topicName, themeName) {
  // first we complete the opinion information
  // eslint-disable-next-line no-param-reassign
  opinion.opinionType = opinionType.THEME;

  // creating the opinion
  const idOpinion = await opinionRepository.insert(opinion);
  // adding the opinion to the event
  await dimensionRepository.addOpinionToTheme(idDimension, topicName, themeName, idOpinion);
  return idOpinion;
}

export default addOpinionToTheme;
