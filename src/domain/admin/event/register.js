async function registerNewEvent({ eventRepository }, event) {
  // eslint-disable-next-line no-param-reassign
  event.isActive = true;
  return await eventRepository.insert(event);
}

export default registerNewEvent;
