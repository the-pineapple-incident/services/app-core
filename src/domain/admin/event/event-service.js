import register from './register';
import getAllActiveEvents from './get-all-active-event';
import addOpinionToEvent from './add-opinion';
import injectDependencies from '../../injection';
import saveAllEvents from './save-extra-events';

const eventService = {
  register: injectDependencies(register),
  getAllActiveEvents: injectDependencies(getAllActiveEvents),
  addOpinion: injectDependencies(addOpinionToEvent),
  saveAllEvents: injectDependencies(saveAllEvents),
};

export default eventService;
