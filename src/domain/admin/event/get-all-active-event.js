async function getAllActiveEvents({ eventRepository }) {
  return await eventRepository.findAllActive();
}

export default getAllActiveEvents;
