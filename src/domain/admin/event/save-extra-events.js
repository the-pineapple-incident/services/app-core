import { events } from '../../../scraping/extra/process/opinions/event/get-events';

async function saveAllEvents({ eventRepository }) {

  const eventRegisterPromises = [];

  events.forEach((event) => {
    const newEvent = { ...event };
    newEvent.period = {
      startDate: newEvent.startDate,
      endDate: newEvent.endDate,
    };
    newEvent.keywords = newEvent.keywords.split(', ');
    newEvent.isActive = true;

    eventRegisterPromises.push(eventRepository.insert(newEvent));
  });

  await Promise.all(
    eventRegisterPromises,
  );
}

export default saveAllEvents;
