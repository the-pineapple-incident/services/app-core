import opinionType from '../../../persistence/models/admin/opinion/opinion-type';

async function addOpinionToEvent({ eventRepository, opinionRepository }, opinion, idEvent) {
  // first we complete the opinion information
  // eslint-disable-next-line no-param-reassign
  opinion.opinionType = opinionType.EVENT;

  // creating the opinion
  const idOpinion = await opinionRepository.insert(opinion);
  // adding the opinion to the event
  await eventRepository.addOpinionToEvent(idEvent, idOpinion);
  return idOpinion;
}

export default addOpinionToEvent;
