import addTopic from './add-topic';
import getTopicsOfDimension from './get-topics-of-dimension';
import injectDependencies from '../../injection';

const topicService = {
  addTopic: injectDependencies(addTopic),
  getTopicsOfDimension: injectDependencies(getTopicsOfDimension),
};

export default topicService;
