async function addTopic({ dimensionRepository }, idDimension, topic) {
  await dimensionRepository.addTopicToDimension(idDimension, topic);
}

export default addTopic;
