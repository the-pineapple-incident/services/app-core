async function getTopicsOfDimension({ dimensionRepository }, idDimension) {
  return await dimensionRepository.getAllTopicsOfDimension(idDimension);
}

export default getTopicsOfDimension;
