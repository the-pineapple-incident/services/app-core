async function getOpinionsOfEvent({ eventRepository }, event) {
  return await eventRepository.getAllOpinionsOfEvent(event.id);
}

export default getOpinionsOfEvent;
