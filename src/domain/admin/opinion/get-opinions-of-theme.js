async function getOpinionsOfTheme({ dimensionRepository }, themeName) {
  return await dimensionRepository.getAllOpinionsOfTheme(themeName);
}

export default getOpinionsOfTheme;
