import injectDependencies from '../../injection';
import getOpinionsOfEvent from './get-opinions-of-event';
import getOpinionsOfTheme from './get-opinions-of-theme';
import getOpinionsOfPoliticFigure from './get-opinions-of-politic-figure';
import saveExtraOpinions from './save-extra-event-opinions';

const opinionService = {
  getAllOpinionsOfEvent: injectDependencies(getOpinionsOfEvent),
  getAllOpinionsOfTheme: injectDependencies(getOpinionsOfTheme),
  getAllOpinionsOfPoliticFigure: injectDependencies(getOpinionsOfPoliticFigure),
  saveAllExtraEventOpinions: injectDependencies(saveExtraOpinions),
};

export default opinionService;
