async function getOpinionsOfPoliticFigure({ opinionRepository }, idPoliticFigure) {
  return await opinionRepository.findOpinionsOfPoliticFigure(idPoliticFigure);
}

export default getOpinionsOfPoliticFigure;
