import moment from 'moment';
import { eventOpinions } from '../../../scraping/extra/process/opinions/event/get-opinions';
import opinionType from '../../../persistence/models/admin/opinion/opinion-type';

async function saveExtraOpinions({ opinionRepository, personRepository }) {

  const findingFiguresPromises = [];

  eventOpinions.forEach((eventOpinion) => {
    findingFiguresPromises.push(personRepository.findFigureId(eventOpinion.candidate_id.substring(3)));
  });

  const res = await Promise.all(
    findingFiguresPromises,
  );

  const eventOpinionsPromises = [];
  for (let i = 0; i < eventOpinions.length; i++) {
    const eventOpinion = eventOpinions[i];
    console.log(res[i]);

    const newOpinion = { ...eventOpinion };
    newOpinion.opinionType = opinionType.EVENT;
    newOpinion.politicFigure = res[i];
    newOpinion.date = moment(newOpinion.date, 'DD-MM-YYYY hh:mm').toDate();

    eventOpinionsPromises.push(opinionRepository.insert(newOpinion));
  }

  await Promise.all(
    eventOpinionsPromises,
  );
}

export default saveExtraOpinions;
