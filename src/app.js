import apiManager from './api/start';
import dbManager from './persistence/start';

async function launch() {

  try {
    await dbManager.startConnection();
    await apiManager.startServer();

  } catch (error) {
    console.error('Cannot launch api', error.message);
    throw error;
  }
}

launch()
  .catch((error) => {
    console.error('Something goes wrong, check logs!', error);
  });
