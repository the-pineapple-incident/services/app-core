import fs from 'fs';

export const read = (completePath) => {
  return JSON.parse(fs.readFileSync(completePath, 'utf8'));
};
