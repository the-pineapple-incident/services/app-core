import path from 'path';
import { read } from '../../../../helpers/file-reader';
import extra from '../../../../../../config/extra';

const events = read(path.join(extra.event.eventsExtraDataPath, 'events.json'));

export { events };
