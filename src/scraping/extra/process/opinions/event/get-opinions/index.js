import path from 'path';
import { read } from '../../../../helpers/file-reader';
import extra from '../../../../../../config/extra';

const eventOpinions = read(path.join(extra.event.eventsExtraDataPath, 'tweets.json'));

export { eventOpinions };
