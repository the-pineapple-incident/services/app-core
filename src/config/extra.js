const extraConfig = {
  politicalParty: {
    logosPath: process.env.EXTRA_INFORMATION_PATH,
  },
  event: {
    eventsExtraDataPath: process.env.EXTRA_INFORMATION_PATH,
  },
};

export default extraConfig;
