import dotenv from 'dotenv';
import { checkBool } from 'helpers/bool';

dotenv.config();

const config = {
  database: {
    name: process.env.DB_NAME,
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
  },
  api: {
    port: process.env.PORT || 3000,
    enablePlayground: checkBool(process.env.ENABLE_GRAPHQL_PLAYGROUND),
    enableIntrospection: checkBool(process.env.ENABLE_GRAPHQL_INTROSPECTION),
  },
};

export default config;
