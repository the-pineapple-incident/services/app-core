import dotenv from 'dotenv';

dotenv.config();

const batchConfig = {
  votoInformado: {
    path: process.env.BATCH_VOTO_INFORMADO_DATA_PATH,
    elections: {
      currentPresidentialEventID: process.env.CURRENT_PRESIDENTIAL_EVENT_ID,
      currentCongresalEventID: process.env.CURRENT_CONGRESAL_EVENT_ID,
    },
  },
  logos: {
    path: process.env.POLITICAL_PARTIES_LOGOS_PATH,
  },
};

export default batchConfig;
