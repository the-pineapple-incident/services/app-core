import { isNil, toLower } from 'ramda';

export const checkBool = (value) => {
  if (isNil(value)) {
    return false;
  }

  return toLower(value) === 'true';
};
