# Only for node image version control
FROM node:12.20-alpine as base
WORKDIR /app


# build with dev dependencies
FROM base as builder
WORKDIR /app

RUN     apk update \
    &&  apk add --no-cache rsync

COPY . .
RUN     yarn install --pure-lockfile --non-interactive \
    &&  yarn build                                     \
    &&  yarn rsync:graphql


# build only with production dependencies
FROM base as dependencies
WORKDIR /app

COPY package.json yarn.lock ./

RUN     NODE_ENV=production \
    &&  yarn install --pure-lockfile --non-interactive --production


# image for production
FROM base as release
WORKDIR /app

COPY --from=builder         /app/build/         ./
COPY --from=dependencies    /app/node_modules/  ./node_modules/

ENV NODE_DEBUG=false
ENV NODE_ENV=production
EXPOSE 3000

CMD ["node", "app.js"]
