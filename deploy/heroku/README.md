# Guide to Deploy to Heroku

Create the heroku app with a cool name (url)
```bash
heroku create <cool-name>
```

Push to heroku repository
```bash
git push heroku master
```

After that, Heroku will execute the following commands, so check if
these commands exist in the `package.json`.
```bash
npm install
npm build
```

Ensure that at least one instance of the app is running scaling to `1`
web dyno
```bash
heroku ps:scale web=1
```
__obs:__ `web` dyno type means that there's an instance that will receive HTTP requests.

Open the url where the app was launch
```bash
heroku open
```

## Configuration
Create the `Procfile` that describes the dyno type, and the command that will be executed
```bash
web: npm start
```
This will be executed after the default commands:
```bash
npm install
npm build
```

### Package.json
Make sure the `package.json` describes the node version to use:
```json
{
  "name": "node-mongodb-beanstalk",
  "version": "1.0.0",
  "engines": {
    "node": "14.x"
  }
}
```

### Config vars
```bash
heroku config:set <key>=<value>
```

Check the env variables:
```bash
heroku config
```

### Run locally
Install the app dependencies and execute heroku locally:
```bash
npm install
heroku local web
```

## Monitoring
Heroku keeps track 1500 lines of logs by default. Check with the following command:
```bash
heroku logs --tail
```

Check how many dynos are running
```bash
heroku ps
```

### Log into dyno
User bash to navigate inside a dyno container:
```bash
heroku run bash
```

## Scaling
Scale it up:
```bash
heroku ps:scale web=1
```
